#!/usr/bin/env bash
# This script allows one to get, in one go, a shell in a container, or run a
# comand in a container.
#
# This is useful when debugging os functions. One creates a .scm file using
# the function, and give this scm file to this script, along with a command
# that will tell whether the intended effects happened or not.
set -euxo pipefail

read -r -d '' USAGE <<'EOF' || true  # Making set -e happy, read will error on EOF because POSIX
test-container.sh (nonet|hostnet|sharednet|vm) <options> <container.scm> [<cmd>]

Starts the OS defined in <container.scm>, run <cmd> in it if provided.

nonet:      No network
hostnet:    container shares its network with the host
sharednet:  container has its own IP
vm:         os is run in a VM.

EOF

if [ "$#" -lt 2 ]; then
    echo "$USAGE"
    exit 1
fi

TYPE="$1"
IFS=' ' read -r -a CONTAINER_ARGS <<< "$2"  #Read the container options in an array
CONTAINER_SCM="$3"
if [ -z ${4+x} ]
then
    CMD=("/run/current-system/profile/bin/bash" "--login")
else
    CMD=("/run/current-system/profile/bin/bash" "-lc" "$4")
fi

case "$TYPE" in
    nonet|sharednet)
        CONTAINER_SCRIPT=$(guix system container "$CONTAINER_SCM")
        ;;
    hostnet)
        CONTAINER_SCRIPT=$(guix system container --network "$CONTAINER_SCM")
        ;;
    vm)
        # In a VM, the --share and --expose options must be provided when
        # creating the VM, not when launching it (because a mount script must
        # exist in the VM to mount the share) so we use CONTAINER_ARGS here.
        CONTAINER_SCRIPT=$(guix system vm --no-graphic "$CONTAINER_SCM" "${CONTAINER_ARGS[@]}")
        ;;
esac
sudo echo Caching sudo passwd


case "$TYPE" in
    vm)
        sudo "$CONTAINER_SCRIPT"  # Not in background because the qemu takes over the tty
        ;;
    nonet|sharednet|hostnet)
        sudo "$CONTAINER_SCRIPT" "${CONTAINER_ARGS[@]}" &
        SUDO_PID=$!
        read
        # ugly, but I can't get the output of (explain pid). I don't understand
        # why. It appears on screen but can't be redirected...
        # So I inspect the few processes spawned by the above command
        # and find the pid of shepherd, which is pid 1 in the container
        pstree -ap > /tmp/tree  # Do it with an intermedary file,
        # so that the grep itself does not appear in the pstree
        CONTAINER_PID=$(grep "$SUDO_PID" -A 5 < /tmp/tree | grep -shepherd, | grep -Eo '[0-9]+' \
            | head -n1)

        function cleanup {
            sudo kill "$CONTAINER_PID"
            sleep 2  # Asking nicely often isn't enough
            sudo kill -9 "$CONTAINER_PID" || true
            if [ "$TYPE" = "sharednet" ];
            then
                sudo ip netns del guix-net
                sudo ip link  del veth-guix
            fi
        }
        trap cleanup EXIT

        # Setting up a shared network as explained in the cookbook
        # https://guix.gnu.org/cookbook/en/html_node/Container-Networking.html
        if [ "$TYPE" = "sharednet" ];
        then
            sudo ip netns attach guix-net "$CONTAINER_PID"
            sudo ip link add veth-guix type veth peer name ceth-guix
            sudo ip link set ceth-guix netns guix-net
            sudo ip link set veth-guix up
            sudo ip addr add 10.0.0.1/24 dev veth-guix
            sudo ip netns exec guix-net ip link set lo up
            sudo ip netns exec guix-net ip link set ceth-guix up
            sudo ip netns exec guix-net ip addr add 10.0.0.2/24 dev ceth-guix
        fi

        sudo guix container exec "$CONTAINER_PID" "${CMD[@]}"
        # Here, the user interacts with the container. When the above command returns,
        # the cleanup should happen because of the trap set earlier.
        ;;
esac
