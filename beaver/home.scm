(define-module (beaver home)
  #:use-module (gnu home)
  #:use-module (gnu services)
  #:use-module (beaver functional-home-services)
  #:use-module (beaver packages plan9)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages lsof)
  #:use-module (gnu packages base)
  #:use-module (gnu packages package-management)
  #:use-module (guix gexp)
  #:export (->
            packages
            home/fish
            home/zsh
            home/mkdir-p
            home/shepherd-service
            home/9p-serve
            home/profile))

;; From https://gist.github.com/emanon-was/ed12f6023e2d6328334a
(define-syntax ->
  (syntax-rules (@)
    ((_ x) x)
    ((_ x (form @ more ...)) (apply form x more ...)) ; list to args, @ chosen for similarity with ,@ in quasiquoted exprs
    ((_ x (form more ...)) (form x more ...))
    ((_ x form) (form x))
    ((_ x form more ...) (-> (-> x form) more ...))))

(define (packages h . packages)
  "Return a copy of h (a home-environment) edited in such a way that the given
are packages available."
  (let ((current-packages (home-environment-packages h)))
    (home-environment
     (inherit h)
     (packages (append current-packages packages)))))

(define (home/fish h)
  "Return a copy of h (a home-environment) edited in such a way that after
login, the user is greeted by a fish shell.

Note that fish is technically not the login shell, as that would require root
intervention."
  (-> h
      (extend-service files
                   `((".bashrc"
                      ,(plain-file "start_fish"
"# From https://wiki.archlinux.org/title/Fish#Setting_fish_as_interactive_shell_only
if [[ $(ps --no-header --pid=$PPID --format=comm) != \"fish\" && -z ${BASH_EXECUTION_STRING} ]]
then
	shopt -q login_shell && LOGIN_OPTION='--login' || LOGIN_OPTION=''
	exec fish $LOGIN_OPTION
fi"))))
      (add-service fish)
      (packages fish)))

(define (home/zsh h)
  "Return a copy of h (a home-environment) edited in such a way that after
login, the user is greeted by a zsh shell.

Note that zsh is technically not the login shell, as that would require root
intervention."
  (-> h
      (extend-service files
                   `((".zshrc"
                      ,(plain-file "start_fish"
                                                   "
# From https://wiki.archlinux.org/title/Zsh#Setting_zsh_as_interactive_shell_only
if [[ $(ps --no-header --pid=$PPID --format=comm) != \"zsh\" && -z ${BASH_EXECUTION_STRING} ]]
then
	shopt -q login_shell && LOGIN_OPTION='--login' || LOGIN_OPTION=''
	exec zsh $LOGIN_OPTION
fi"))))
      (add-service zsh)
      (packages zsh)))

(define* (home/mkdir-p h path #:key mode (owner -1) (group -1))
  "Return a copy of H (an home-environment) in which there is a directory at
PATH, optionally chmoded to MODE and/or chowned to OWNER."
    (extend-service
     h
     activation
     (with-imported-modules '((guix build utils))
       #~(begin
           (use-modules (guix build utils))
           (mkdir-p #$path)
         (when #$mode
           (chmod #$path #$mode))
         (chown #$path
                (if (string? #$owner)
                    (vector-ref (getpw #$owner) 2)
                    #$owner)  ;; Assume numeric uid
                (if (string? #$group)
                    (vector-ref (getgr #$group) 2)
                    #$group))))))  ;; Assume numeric gid

(define* (home/shepherd-service h cmd #:key (cwd #f) (name #f))
  "Return a copy of H, adding a shepherd service that launches CMD in CWD.

CMD is a shell line, given to bash -c after sourcing the home profile."
  (let* [(name (or name (car (string-split cmd #\ ))))
         (home-directory (or (getenv "HOME") (passwd:dir passwd)))
         (cwd (or cwd home-directory))]
    (->
     h
     (home/mkdir-p (string-append home-directory "/var/log"))
     ((lambda (x)
        (extend-service
         x
         shepherd
         (list (shepherd-service
                (documentation (string-append "Run the " name " daemon"))
                (requirement '())
                (provision (list (string->symbol name)))
                (start #~(make-forkexec-constructor
                          (list #$(file-append
                                   bash
                                   "/bin/bash") "-c" (string-append
                                                        "cd " #$cwd "; "
                                                        "source ~/.guix-home/profile/etc/profile; "
                                                        #$cmd
                                                        " > " #$home-directory "'/var/log/"
                                                        #$name ".log' 2>&1"))))
                (stop #~(make-kill-destructor))))))))))

(define* (home/9p-serve h dir _socket #:key
                       (read-write "0")
                       (group "users")
                       (mode "700") (name #f))
  "Return a copy of H, adding a shepherd service that runs a 9P server
serving DIR on _SOCKET.

Access control works by setting READ-WRITE to #t only if necessary, and
choosing the appropriate GROUP and MODE for the socket.

There is no 9P authentication. Users able to rw the socket will be able to act
on your behalf via the 9P server."
  (let* [(home-directory (or (getenv "HOME") (passwd:dir passwd)))
         (dir (if (char=? (string-ref dir 0) #\/)
                  dir
                  (string-append home-directory "/" dir)))]
    (->
     h
     (home/mkdir-p dir)
     (home/mkdir-p (dirname _socket))
     (home/mkdir-p (string-append home-directory "/var/log/"))
     (packages go-github-com-hugelgupf-p9-cmd-p9ufs lsof coreutils)
     (extend-service
      shepherd
      (list (shepherd-service
             (documentation (string-append "Serve " dir " on " _socket))
             (requirement '())
             (provision (list name))
             (start #~(make-forkexec-constructor
                       (list
                        #$(file-append bash "/bin/bash")
                        #$(plain-file "9pserve.sh" "
# It is kind of odd that we have to source the user's profile
# I would have expected it to be the default profile for shepherd services
.  ~/.guix-home/profile/etc/profile
set -euxo pipefail
dir=\"$1\"
socket=\"$2\"
read_write=\"$3\"
group=\"$4\"
mode=\"$5\"

cd \"$dir\"
if [ -S \"$socket\" ]
then
    if ! lsof \"$socket\" > /dev/null
    then
        # Socket exists but nobody is listening on it: remove it
        rm -f \"$socket\"
    else
        echo Socket $socket exists and somebody is listening on it. Killing myself
        exit 1
    fi
fi
# At this point the socket does not exist, unless there is a race condition
if [ \"$read_write\" == \"0\" ]
then
    container_arg=\"--expose=$dir\"
else
    container_arg=\"--share=$dir\"
fi
guix shell --container \"$container_arg\" \
    --share=\"$(dirname \"$socket\")\" \
    --profile=\"$HOME/.guix-home/profile\" -- \
    p9ufs --root=\"$dir\" -v --unix \"$socket\" &
p9ufs_pid=$!
cleanup () {
     kill -9 \"$p9ufs_pid\"
}
trap cleanup EXIT
sleep 1;
if ! [ -S \"$socket\" ];
then
   sleep 10;
   if ! [ -S \"$socket\" ]
   then
      echo The socket $socket did not appear, killing myself
      exit 1
   fi
fi
chown \"$USER:$group\" \"$socket\"
chmod \"$mode\" \"$socket\"
echo $p9ufs_pid > \"$socket\".pid
wait
")
                       #$dir
                       #$_socket
                       #$read-write
                       #$group
                       #$mode
                       )
                      ;; Probably redundant with the chown in 9pserve.sh
                      #:pid-file #$(string-append _socket ".pid")
                      #:pid-file-timeout 15  ;; The default is too short, the
                                             ;; service is killed during the
                                             ;; 'sleep 10'.
                      #:log-file #$(string-append home-directory "/var/log/" (symbol->string name) ".log")
                      ))
            (stop #~(make-kill-destructor))))))))

(define* (home/profile h profile #:key (packages '()) (name #f))
  "Return a copy of H edited in such a way that the given
PACKAGES are available in the profile at PROFILE."
  (let* [(home-directory (or (getenv "HOME") (passwd:dir passwd)))
         (profile (if (char=? (string-ref profile 0) #\/)
                  profile
                  (string-append home-directory "/" profile)))
         (name (or name
                   (string->symbol (string-append "profile:" (basename profile)))))]
    (extend-service
     h
     shepherd
     (list (shepherd-service
            (documentation (string-append "Create profile: " profile))
            (requirement '())
            (provision (list name))
            (one-shot? #t)
            (start #~(lambda _
                             (invoke (string-append #$home-directory "/.config/guix/current/bin/guix")
                                     "install" (string-append "--profile=" #$profile)
                                     #$@packages))))))))
