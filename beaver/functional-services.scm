(define-module (beaver functional-services)
   #:use-module (gnu system)
   #:use-module (gnu services)
   #:export (add-service extend-service modify-service remove-service
                         service-configuration))

(define syntax->string (compose symbol->string syntax->datum))

(define (service-configuration-stx stx service)
  "Return the syntax one can use to refer to xxx-configuration for the given
service"
  (datum->syntax stx (string->symbol
                      (string-append
                       (syntax->string service)
                       "-configuration"))))

(define (service-type stx service)
  "Return the syntax one can use to refer to xxx-service-type for the given
service"
  (datum->syntax stx (string->symbol
                      (string-append
                       (syntax->string service)
                       "-service-type"))))

(define-syntax add-service
  (lambda (stx)
    (syntax-case stx ()
      [(_ os service-name)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)  ;; It is wrapped in a lamba to make sure os is
               ;; evaluated once only. It it wasn't in a labmda, whatever
               ;; form os is in the calling code would be repeated
               ;; multiple times, and so if the form was e.g. (some-func
               ;; os), then some-func would be called multiple times,
               ;; which may not be desirable.
               (operating-system
                 (inherit x)
                 (services
                  (cons
                   (service service-type)
                   (operating-system-user-services x)))))
             os)))]
      [(_ os service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)]
         [service-configuration (service-configuration-stx stx #'service-name)])
        #'(begin
            ((lambda (x)  ;; Wrapping in a lambda for the same reasons as above
               (operating-system
                 (inherit x)
                 (services
                  (cons
                   (service service-type
                                 (service-configuration forms ...))
                   (operating-system-user-services x)))))
             os)))])))

(define-syntax extend-service
  (lambda (stx)
    (syntax-case stx ()
      [(_ os service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)
               (operating-system
                 (inherit x)
                 (services
                  (cons
                   (simple-service (format  #f "A ~a extension" (syntax->string #'service-name))
                                   service-type
                                   forms ...)
                   (operating-system-user-services x)))))
             os)))])))

(define-syntax service-configuration
  (lambda (stx)
    (syntax-case stx ()
      [(_ os service-name)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)
               (service-value
                (car
                 (filter
                  (lambda (service)
                    (eq? (service-kind service) service-type))
                  (operating-system-user-services x)))))
             os)))])))

(define-syntax modify-service
  (lambda (stx)
    (syntax-case stx (=>)
      [(_ os service-name conf-var => forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)]
         [service-configuration-stx (service-configuration-stx stx #'service-name)])
        #'(begin
            ((lambda (x)
               (let ([conf-var (service-configuration x service-name)])
                 (operating-system
                   (inherit x)
                   (services
                    (modify-services (operating-system-user-services x)
                      (service-type
                       config =>
                       (service-configuration-stx
                        (inherit config)
                        forms ...)))))))
             os)))]
      [(_ os service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)]
         [service-configuration (service-configuration-stx stx #'service-name)])
        #'(begin
            ((lambda (x)
               (operating-system
                 (inherit x)
                 (services
                  (modify-services (operating-system-user-services x)
                    (service-type
                     config =>
                     (service-configuration
                      (inherit config)
                      forms ...))))))
             os)))]
       )))

(define-syntax remove-service
  (lambda (stx)
    (syntax-case stx ()
      [(_ os service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)
               (operating-system
                 (inherit x)
                 (services
                  (modify-services (operating-system-user-services x)
                    (delete service-type)))))
             os)))])))
