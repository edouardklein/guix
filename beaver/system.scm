(define-module (beaver system)
  #:use-module (guix gexp)
  #:use-module (guix modules)
  #:use-module (guix channels)
  #:use-module (guix records)
  #:use-module (guix ci)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services)
  #:use-module (gnu services web)
  #:use-module (gnu services base)
  #:use-module (gnu services certbot)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services ssh)
  #:use-module (gnu services networking)
  #:use-module (gnu services telephony)
  #:use-module (gnu services sysctl)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system shadow)
  #:use-module (gnu system setuid)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages lsof)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages version-control)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 match)
  #:use-module (gcrypt hash)
  #:use-module (rnrs bytevectors)
  #:use-module (beaver functional-services)
  #:use-module (beaver packages plan9)
  #:use-module (beaver packages unix)
  #:export (%beaverlabs-channels
            %beaverlabs+nonguix-channels
            beaverlabs-channels
            beaverlabs+nonguix-channels
            add-hpc-channel
            minimal-container
            very-minimal-container
            minimal-ovh
            openssh-root-key
            packages
            http-reverse-proxy
            %cert-fullchain
            %cert-privkey
            https-reverse-proxy
            httpx-reverse-proxy
            http-static-content
            https-static-content
            httpx-static-content
            mumble
            users
            groups
            os/mkdir-p
            os/file
            nobody-like-user
            ssh-user
            os/setuid
            os/setcap
            suc-private-channel
            suc-public-channel
            suc-dropbox-channel
            os/git
            os/hostname
            os/permaudit
            os/9mount
            os/mount.9p
            join-the-tilde-club
            os/listen
            os/shepherd-service
            listen/echo
            os/profile
            os/9p-serve
            listen/finger
            listen/finger/hello
            listen/finger/inspect
            listen/finger/snail
            listen/finger/user-default
            listen/git-daemon
            ->
            ))
(define %beaverlabs-channels
  ;; Channels that should be available to
  ;; /run/current-system/profile/bin/guix.
  (list (channel
         (name 'beaverlabs)
         (url "https://gitlab.com/edouardklein/guix")
         (branch "beaverlabs"))
        (channel
         (name 'guix)
         (url "https://git.savannah.gnu.org/git/guix.git")
         (branch "master")
         (introduction
          (make-channel-introduction
           "9edb3f66fd807b096b48283debdcddccfea34bad"
           (openpgp-fingerprint
            "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))))

(define %beaverlabs+nonguix-channels
  ;; Channels that should be available to
  ;; /run/current-system/profile/bin/guix.
  (cons (channel
         (name 'nonguix)
         (url "https://gitlab.com/nonguix/nonguix")
         (introduction
          (make-channel-introduction
           "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
           (openpgp-fingerprint
            "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
        %beaverlabs-channels))

(define (beaverlabs-channels os)
  "Add beaverlabs channels by default. Without this, it won't remain after a reconfigure"
  (modify-service os guix
                  (channels %beaverlabs-channels)
                  (guix (guix-for-channels %beaverlabs-channels))))

(define (beaverlabs+nonguix-channels os)
  "Add beaverlabs channels by default. Without this, it won't remain after a reconfigure"
  (modify-service os guix
                  (channels %beaverlabs+nonguix-channels)
                  (guix (guix-for-channels %beaverlabs+nonguix-channels))))

(define (add-hpc-channel os)
  "Add the guix-hpc channel"
  (let ((%channels
         (cons (channel
                (name 'guix-hpc)
                (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git"))
               (guix-configuration-channels (service-configuration os guix))))
        (%substitute-urls
         (cons "https://guix.bordeaux.inria.fr"
               (guix-configuration-substitute-urls (service-configuration os guix))))
        (%authorized-keys
         (cons (plain-file "guix-hpc.pub"
                           "(public-key
 (ecc
  (curve Ed25519)
  (q #89FBA276A976A8DE2A69774771A92C8C879E0F24614AAAAE23119608707B3F06#)))")
               (guix-configuration-authorized-keys (service-configuration os guix)))))
    (modify-service os guix
                    (channels %channels)
                    (guix (guix-for-channels %channels))
                    (authorized-keys %authorized-keys)
                    (substitute-urls %substitute-urls))))

;; This operating system does nothing, but contains
;; all that is needed to run other services on it.
;; It could (FIXME:should!) probably be made leaner
;; by removing unneeded utilities, documentation, the guix daemon, etc.
(define minimal-container
  (operating-system
    (host-name "minimal-container")
    (timezone "UTC")
    (locale "en_US.utf8")
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)))
    (file-systems %base-file-systems)
    (users %base-user-accounts)
    (packages %base-packages)
    (services %base-services)))

;; From https://gist.github.com/emanon-was/ed12f6023e2d6328334a
(define-syntax ->
  (syntax-rules (@)
    ((_ x) x)
    ((_ x (form @ more ...)) (apply form x more ...)) ; list to args, @ chosen for similarity with ,@ in quasiquoted exprs
    ((_ x (form more ...)) (form x more ...))
    ((_ x form) (form x))
    ((_ x form more ...) (-> (-> x form) more ...))))

(define very-minimal-container
  (-> minimal-container
      (remove-service guix)
      (remove-service udev)
      (remove-service urandom-seed)))

(define (openssh-root-key os pubkeyfile)
  "Return a copy of OS (an operating-system) modified in such a way that an
openssh server is started, allowing root login with the given public key (in a
file-like object)."
  (add-service os openssh
   (permit-root-login 'prohibit-password)
   (password-authentication? #f)
   (x11-forwarding? #t)
   (authorized-keys
    `(("root" ,pubkeyfile)))))

(define minimal-ovh
  ;; This function of one argument returns an operating system that does
  ;; nothing, but contains all the necessary boilerplate to be run on an OVH
  ;; VPS and have other services added to it.
  ;;
  ;;Its only argument SSH-PUBLIC-KEY can be a string (of the form,
  ;;e.g. ssh-rsa AAAAB3...fPShQ== toto) or a file-like object.
  ;;
  ;; Note that when called with no argument, no ssh server will be started by
  ;; the returned operating-system and you need to add one by chaining the
  ;; call to MINIMAL-OVH with e.g. OPENSSH-ROOT-KEY or SSH-USER, if you want
  ;; to be able to connect to your VPS.
  (match-lambda*
    (((? string? ssh-public-key))
     (->
      (minimal-ovh)  ;; Recursive call with no args
      (openssh-root-key (plain-file "root.pub" ssh-public-key))))
    ((ssh-public-key)  ;; Assume file-like object
     (->
      (minimal-ovh)  ;; Recursive call with no args
      (openssh-root-key ssh-public-key)))
    (() ;; No args, return the base OS with no ssh server
     (->
      (operating-system
        (locale "en_US.utf8")
        (timezone "UTC")
        (keyboard-layout (keyboard-layout "fr"))
        (host-name "minimal-ovh")
        (bootloader
         (bootloader-configuration
          (bootloader grub-bootloader)
          (targets '("/dev/sda"))
          (keyboard-layout keyboard-layout)))
        (initrd-modules
         (append '("virtio_scsi") %base-initrd-modules))
        (file-systems
         (cons (file-system
                 (mount-point "/")
                 (device
                  (if (access? "/dev/sda2" F_OK)
                      ;; As of 2022-08-28 OVH vps don't have a bios boot partition anymore
                      ;; but support is needed for VPSs created before this date
                     "/dev/sda2" "/dev/sda1"))
                 (type "ext4"))
               %base-file-systems)))
      (add-service dhcp-client)))))

(define (packages os . packages)
  "Return a copy of OS (an operating-system) edited in such a way that the given
packages are globally available

FIXME: Use a profile-service-type, e.g. add
 (service-extension profile-service-type
                                          (const (list package1 etc.)))
to the os' services.
"
  (operating-system
    (inherit os)
    (packages
     (append packages (operating-system-packages os)))))

(define* (users os #:rest users)
  "Return a copy of OS (an operating-system) edited in such a way that the given
users exist on the system.

Users can be given as usernames (strings), or user-accounts records.

If a user with the same name as provided already exist, no modification is made
to the system, even if the other fields of the user-account record differ."
  (operating-system
    (inherit os)
    (users
     (append
      (filter (lambda (x) x)  ; Remove #f entries
              (map (match-lambda
                     ((? string? username)
                      (if (member username
                                  (map user-account-name (operating-system-users os)))
                          #f
                          (user-account
                           (name username)
                           (group "users")
                           (home-directory (string-append "/home/" username)))))
                     ((? user-account? account)
                      (if (member (user-account-name account)
                                  (map user-account-name (operating-system-users os)))
                          #f
                          account)))
                   users))
      (operating-system-users os)))))

(define* (nobody-like-user os username #:key (home "/var/empty"))
  "Return a copy of OS (an operating-system) edited in such a way that a user
named username exists on the system.

This user owns very little on the system, and as such is suitable for owning processes
which shall not be able to impact the system.

If a user of same name already exist on the system, nothing is done."
  (if (member username
              (map user-account-name (operating-system-users os)))
      os
      (users os (user-account
                 (name username)
                 (group username)
                 (home-directory home)
                 (shell (file-append shadow "/sbin/nologin"))))))

(define* (groups os #:rest groups)
   "Return a copy of OS (an operating-system) edited in such a way that the given
groups exist on the system.

Groups can be given as groupnames (strings), or user-group records.

If a group with the same name as provided already exist, no modification is made
to the system, even if the other fields of the user-group record differ."
  (operating-system
    (inherit os)
    (groups
     (append
      (filter (lambda (x) x)  ; Remove #f entries
              (map (match-lambda
                     ((? string? groupname)
                      (if (member groupname
                                  (map user-group-name (operating-system-groups os)))
                          #f
                          (user-group (name groupname))))
                     ((? user-group? group)
                      (if (member (user-group-name group)
                                  (map user-group-name (operating-system-groups os)))
                          #f
                          group)))
                   groups))
      (operating-system-groups os)))))

(define* (os/mkdir-p os path #:key mode (owner -1) (group -1))
  "Return a copy of OS (an operating system) in which there is a directory at
PATH, optionally chmoded to MODE and/or chowned to OWNER."
  (extend-service
   os
   activation
   (with-imported-modules '((guix build utils))
     #~(begin
         (use-modules (guix build utils))
         (mkdir-p #$path)
         (when #$mode
           (chmod #$path #$mode))
         (chown #$path
                (if (string? #$owner)
                    (vector-ref (getpw #$owner) 2)
                    #$owner)  ;; Assume numeric uid
                (if (string? #$group)
                    (vector-ref (getgr #$group) 2)
                    #$group))))))  ;; Assume numeric gid

(define* (os/file os path #:key mode (owner -1) (group -1))
  "Return a copy os OS (an operating system) in which there is a file at PATH,
optionally chmoded to MODE and/or chowned to OWNER.

If the file already exists, its contents are left untouched, but it is chowned
and chmoded."
  (extend-service
   os
   activation
   (with-imported-modules '((guix build utils))
     #~(begin
         (use-modules (guix build utils))
         (unless (stat #$path #f)
           (mkdir-p (dirname #$path))
           (mknod #$path 'regular (if #$mode #$mode #o640) 0))
         (when #$mode
           (chmod #$path #$mode))
         (chown #$path
                (if (string? #$owner)
                    (vector-ref (getpw #$owner) 2)
                    #$owner)  ;; Assume numeric uid
                (if (string? #$group)
                    (vector-ref (getgr #$group) 2)
                    #$group)))))) ;; Assume numeric gid

(define os/groups groups)
;; Needed below because the keyword argument to ssh-user shadows the function
(define* (ssh-user os user #:key (groups '()) keys)
  "Return a copy of OS (an operating-system) edited in such a way that the given
user exists on the system, with the optionally given supplementary groups, and
with the given key file names or file-like objects as SSH public keys."
  (-> os
      (users
       (user-account
        (name user)
        (group "users")
        (supplementary-groups (map (match-lambda
                                     ((? string? groupname) groupname)
                                     ((? user-group? group) (user-group-name group)))
                                   groups))
        (home-directory (string-append "/home/" user))))
      (os/groups @ groups)
      (extend-service
       openssh
       (map (lambda (keyfile)
              `(,user ,(if (string? keyfile)
                           (local-file keyfile)
                           keyfile)))
            keys))))

(define* (http-reverse-proxy os #:key 
                             (from-host "localhost")
                             (from-port 80)
                             (to-host "localhost")
                             (to-port 8000)
                             (raw-content '())
                             (loc-conf-strings '()))
  "Return a copy of OS (an operating-system) edited in such a way that nginx is
reverse-proxying the connections it receives at FROM-HOST:FROM-PORT to
TO-HOST:TO-PORT."
  (extend-service
   os
   nginx
   (list
    (nginx-server-configuration
     (listen `(,(format #f "~a" from-port)))
     (server-name `(,from-host))
     (locations
      (list
       (nginx-location-configuration
        (uri "/")
        (body `(,(format #f "proxy_pass http://~a:~a;"
                         to-host to-port)
                ,(format #f "error_log /var/log/nginx/~a.error.log error;" from-host)
                ,(format #f "access_log /var/log/nginx/~a.access.log;" from-host)
                ,@loc-conf-strings)))))
     (raw-content raw-content)))))

;; %cert-* functions allow for the users to parametrize the places
;; where the ssl files are, without having to pass them as arguments everywhere
(define %cert-fullchain (make-parameter #f))
(define %cert-privkey (make-parameter #f))

(define* (https-reverse-proxy os #:key
                              from-host
                              (fullchain-path #f)
                              (privkey-path #f)
                              (from-port 443)
                              (to-host "localhost")
                              (to-port 8000)
                              (raw-content '())
                              (loc-conf-strings '()))
  "Return a copy of OS (an operating-system) edited in such a way that nginx is
reverse-proxying the https connections it receives at FROM-HOST:FROM-PORT to
TO-HOST:TO-PORT.

Unless specified otherwise, ssl certs and key files are looked for in
/etc/ssl/certs/FROM-HOST/fullchain.cer and
/etc/ssl/private/FROM-HOST/privkey.pem, respectively.

The path can be passed as strings, or as functions of the domain name that
should return a string, or via the %cert-fullchain or %cert-privkey
functions."
  (let ([fullchain-path
         (cond
          [(string? fullchain-path) fullchain-path]
          [(procedure? fullchain-path) (fullchain-path from-host)]
          [(%cert-fullchain) ((%cert-fullchain) from-host)]
          [else (string-append "/etc/ssl/certs/" from-host "/fullchain.cer")])]
        [privkey-path
         (cond
          [(string? privkey-path) privkey-path]
          [(procedure? privkey-path) (privkey-path from-host)]
          [(%cert-privkey) ((%cert-privkey) from-host)]
          [else (string-append "/etc/ssl/certs/" from-host "/privkey.pem")])])
    (extend-service
     os
     nginx
     (list (nginx-server-configuration
            (listen `(,(format #f "~a ssl" from-port)))
            (server-name `(,from-host))
            (locations
             (list
              (nginx-location-configuration
               (uri "/")
               (body `(,(format #f "proxy_pass http://~a:~a;"
                                to-host to-port)
                       ,(format #f "error_log /var/log/nginx/~a.error.log error;" from-host)
                       ,(format #f "access_log /var/log/nginx/~a.access.log;" from-host)
                       ,@loc-conf-strings)))))
            (raw-content raw-content)
            (ssl-certificate fullchain-path)
            (ssl-certificate-key privkey-path))))))


(define* (httpx-reverse-proxy os #:key
                              from-host
                              (fullchain-path #f)
                              (privkey-path #f)
                              (to-host "localhost")
                              (to-port 8000)
                              (raw-content '())
                              (loc-conf-strings '()))
  "Return a copy of OS (an operating-system) edited in such a way that nginx is
reverse-proxying the https and http connections it receives at
FROM-HOST:80 (http) and FROM-HOST:443 (https) to TO-HOST:TO-PORT.

Unless specified otherwise, ssl certs and key files are looked for in
/etc/ssl/certs/FROM-HOST/fullchain.cer and
/etc/ssl/private/FROM-HOST/privkey.pem, respectively.

The path can be passed as strings, or as functions of the domain name that
should return a string, or via the %cert-fullchain or %cert-privkey
functions."
    (-> os
        (https-reverse-proxy #:from-host from-host
                             #:fullchain-path fullchain-path
                             #:privkey-path privkey-path
                             #:to-host to-host
                             #:to-port to-port
                             #:raw-content raw-content
                             #:loc-conf-strings loc-conf-strings)
        (http-reverse-proxy #:from-host from-host
                            #:to-host to-host
                            #:to-port to-port
                            #:raw-content raw-content
                            #:loc-conf-strings loc-conf-strings)))

(define* (http-static-content os #:key
                              from-host
                              (from-port 80)
                              (to-dir "/var/www")
                              (allow-dir-listing #f)
                              (raw-content '())
                              (loc-conf-strings '())
                              (nomkdir #f))
  "Return a copy of OS (an operating-system) edited in such a way that nginx is serving static content from TO-DIR on FROM-HOST:FROM-PORT."
  (->
   os
   (extend-service
    nginx
    (list (nginx-server-configuration
           (listen `(,(format #f "~a" from-port)))
           (server-name `(,from-host))
           (locations
            (list
             (nginx-location-configuration
              (uri "/")
              (body `(,(format #f "root ~a;" to-dir)
                      "index index.html;"
                      ,(format #f "error_log /var/log/nginx/~a.error.log error;" from-host)
                      ,(format #f "access_log /var/log/nginx/~a.access.log;" from-host)
                      ,(string-append "autoindex " (if allow-dir-listing "on" "off") ";")
                      ,@loc-conf-strings)))))
           (raw-content raw-content))))
   ((lambda (os)
      (if nomkdir
          os
          (os/mkdir-p os to-dir #:mode #o551 #:owner "nginx" #:group "nginx"))))))

(define* (https-static-content os #:key
                              from-host
                              (fullchain-path #f)
                              (privkey-path #f)
                              (from-port 443)
                              (to-dir "/var/www")
                              (allow-dir-listing #f)
                              (raw-content '())
                              (loc-conf-strings '())
                              (nomkdir #f))
  "Return a copy of OS (an operating-system) edited in such a way that nginx is
serving static content from TO-DIR on FROM-HOST:FROM-PORT.

Unless specified otherwise, ssl certs and key files are looked for in
/etc/ssl/certs/FROM-HOST/fullchain.cer and
/etc/ssl/private/FROM-HOST/privkey.pem, respectively.

The path can be passed as strings, or as functions of the domain name that
should return a string, or via the %cert-fullchain or %cert-privkey
functions."
  (let ([fullchain-path
         (cond
          [(string? fullchain-path) fullchain-path]
          [(procedure? fullchain-path) (fullchain-path from-host)]
          [(%cert-fullchain) ((%cert-fullchain) from-host)]
          [else (string-append "/etc/ssl/certs/" from-host "/fullchain.cer")])]
        [privkey-path
         (cond
          [(string? privkey-path) privkey-path]
          [(procedure? privkey-path) (privkey-path from-host)]
          [(%cert-privkey) ((%cert-privkey) from-host)]
          [else (string-append "/etc/ssl/certs/" from-host "/privkey.pem")])])
    (->
     os
     (extend-service
      nginx
      (list (nginx-server-configuration
             (listen `(,(format #f "~a ssl" from-port)))
             (server-name `(,from-host))
             (locations
              (list
               (nginx-location-configuration
                (uri "/")
                (body `(,(format #f "root ~a;" to-dir)
                        "index index.html;"
                        ,(format #f "error_log /var/log/nginx/~a.error.log error;" from-host)
                        ,(format #f "access_log /var/log/nginx/~a.access.log;" from-host)
                        ,(string-append "autoindex " (if allow-dir-listing "on" "off") ";")
                        ,@loc-conf-strings)))))
             (raw-content raw-content)
             (ssl-certificate fullchain-path)
             (ssl-certificate-key privkey-path))))
     ((lambda (os)
        (if nomkdir
            os
            (os/mkdir-p os to-dir #:mode #o551 #:owner "nginx" #:group "nginx")))))))

(define* (httpx-static-content os #:key
                               from-host
                               (fullchain-path #f)
                               (privkey-path #f)
                               (to-dir "/var/www")
                               (allow-dir-listing #f)
                               (raw-content '())
                               (loc-conf-strings '())
                               (nomkdir #f))
  "Return a copy of OS (an operating-system) edited in such a way that nginx is
serving static content from TO-DIR on FROM-HOST:80 (http) and FROM-HOST:443 (https).

Unless specified otherwise, ssl certs and key files are looked for in
/etc/ssl/certs/FROM-HOST/fullchain.cer and
/etc/ssl/private/FROM-HOST/privkey.pem, respectively.

The path can be passed as strings, or as functions of the domain name that
should return a string, or via the %cert-fullchain or %cert-privkey
functions."
  (-> os
      (https-static-content #:from-host from-host
                            #:fullchain-path fullchain-path
                            #:privkey-path privkey-path
                            #:to-dir to-dir
                            #:allow-dir-listing allow-dir-listing
                            #:raw-content raw-content
                            #:loc-conf-strings loc-conf-strings
                            #:nomkdir nomkdir)
      (http-static-content #:from-host from-host
                           #:to-dir to-dir
                           #:raw-content raw-content
                           #:allow-dir-listing allow-dir-listing
                           #:loc-conf-strings loc-conf-strings
                           #:nomkdir nomkdir)))
(define* (mumble os #:key
                 (welcome "Mumble Mumble")
                 (password "hunter2")
                 (domain #f)
                 (fullchain-path #f)
                 (privkey-path #f)
                 (port 64738))
  " Return a copy of OS (an operating-system) edited in such a way that a mumble
server is running.

Unless specified otherwise, ssl certs and key files are looked for in
/etc/ssl/certs/FROM-HOST/fullchain.cer and
/etc/ssl/private/FROM-HOST/privkey.pem, respectively.

The path can be passed as strings, or as functions of the domain name that
should return a string, or via the %cert-fullchain or %cert-privkey
functions."
  (let ([fullchain-path
         (cond
          [(string? fullchain-path) fullchain-path]
          [(procedure? fullchain-path) (fullchain-path domain)]
          [(%cert-fullchain) ((%cert-fullchain) domain)]
          [else (string-append "/etc/ssl/certs/" domain "/fullchain.cer")])]
        [privkey-path
         (cond
          [(string? privkey-path) privkey-path]
          [(procedure? privkey-path) (privkey-path domain)]
          [(%cert-privkey) ((%cert-privkey) domain)]
          [else (string-append "/etc/ssl/certs/" domain "/privkey.pem")])])
    (add-service
     os
     mumble-server
     (welcome-text welcome)
     (port port)
     (server-password password)
     (ssl-cert fullchain-path)
     (ssl-key privkey-path))))

(define %setuid-nb 0)
;; Dirty hack to have os/setuid register a different symbol for each setuid service it adds

(define* (os/setuid os program #:key (mode #o555) (owner 0) (group 0)
                    (setuid? #t) (setgid? #f) (target #f))
  "Return a copy os OS (an operating system) in which the given PROGRAM is
copied to /run/privileged/bin owned by OWNER:GROUP, with base mode MODE and has
its setuid (and optionally its setgid) bit set."
  ;; FIXME The whole setuid machinery is duplicated here, because of this bug
  ;; https://issues.guix.gnu.org/63904
  ;; which should be solved once this
  ;; https://issues.guix.gnu.org/62726
  ;; is merged, at which point we may fall back to extend-service setuid-program
  ;; with the caveat that the target keyword argument of this function
  ;; is not available in upstream's setuid machinery yet.
  (let ([provides (string->symbol (string-append "setuid-program-" (format #f "~a" %setuid-nb)))])
    (set! %setuid-nb (+ 1 %setuid-nb))
    ;(format #t "program is ~a, target is ~a, setuid nb is now ~a, provides is ~a~%" program target %setuid-nb provides)
    (extend-service
     os
     shepherd-root
     (with-imported-modules (source-module-closure
                             '((guix build utils)))
       (list (shepherd-service
              (documentation (format #f "setuid ~a" %setuid-nb))
              (provision `(,provides))
              (requirement '(file-systems))
              (one-shot? #t)
              (modules '((guix build utils)))
              (start #~(lambda ()
                         (let ([target (string-append "/run/privileged/bin"
                                                      "/" (if #$target #$target (basename #$program)))]
                               [mode (+ #$mode
                                         (if #$setuid? #o4000 0)
                                       (if #$setgid? #o2000 0))]
                               [owner (if (string? #$owner)
                                          (vector-ref (getpw #$owner) 2)
                                          #$owner)] ;; Assume numeric uid
                               [group (if (string? #$group)
                                          (vector-ref (getgr #$group) 2)
                                          #$group)]) ;; Assume numeric gid
                           (copy-file #$program target)
                           (chown target owner group)
                           (chmod target mode))))))))))

(define* (os/setcap os program capstring #:key (mode #o555) (owner 0) (group 0)
                    (target #f))
  "Return a copy os OS (an operating system) in which the given PROGRAM is
copied to /run/privileged/bin owned by OWNER:GROUP, with mode MODE and has
setcap CAPSTRING run on it.
This is very similar to what is done in os/setuid."
  (let ([provides (string->symbol (string-append "setcap-program-" (format #f "~a" %setuid-nb)))])
    (set! %setuid-nb (+ 1 %setuid-nb))
    (extend-service
     os
     shepherd-root
     (with-imported-modules (source-module-closure
                             '((guix build utils)))
       (list (shepherd-service
              (documentation (format #f "setcap ~a" %setuid-nb))
              (provision `(,provides))
              (requirement '(file-systems))
              (one-shot? #t)
              (modules '((guix build utils)))
              (start #~(lambda ()
                         (let ([target (string-append "/run/privileged/bin"
                                                      "/" (if #$target #$target (basename #$program)))]
                               [owner (if (string? #$owner)
                                          (vector-ref (getpw #$owner) 2)
                                          #$owner)] ;; Assume numeric uid
                               [group (if (string? #$group)
                                          (vector-ref (getgr #$group) 2)
                                          #$group)]) ;; Assume numeric gid
                           (copy-file #$program target)
                           (chown target owner group)
                           (chmod target #$mode)
                           (invoke (string-append #+libcap "/sbin/setcap") #$capstring target))))))))))


  (define* (suc-private-channel os group name)
    "Return a copy of OS (an operating-system) edited in such a way that a private
suc channel named NAME exists: only the members of GROUP will be able to read
or write to it."
    (suc-channel os group group name))

  (define* (suc-dropbox-channel os group name)
    "Return a copy of OS (an operating-system) edited in such a way that a dropbox
suc channel named NAME exists: only members of GROUP will be able to read from
it, but any member of the suc group will be able to write to it (typically to
request access)."
    (suc-channel os "suc" group name))

  (define* (suc-public-channel os name)
    "Return a copy of OS (an operating-system) edited in such a way that a public
suc channel named NAME exists. Any member of the suc group will be able to
read and write to it."
    (suc-channel os "suc" "suc" name))

  (define* (suc-channel os writers readers name)
    "Return a copy of OS (an operating-system) edited in such a way that a suc
channel named NAME exists. Members of group WRITERS will be able to write to
it, members of group READERS will be able to read from it."
  (let ([suc-exe-target-name
         (if (string= writers "suc")
             "suc"
             (format #f "suc_~a" writers))])
    (->
     os
     (packages suc)    ;; Install suc for all users
     (groups writers)  ;; Ensure the writers and readers group exist
     (groups readers)  ;; Do it in two calls to ensure the deduplication mechanism applies
     (nobody-like-user writers)  ;; Ensure a user named like the writers group exists
     ;; Create (if need be), chown, and chmod the channel file
     (os/file (format #f "/var/lib/suc/~a" name) #:mode #o640 #:owner writers #:group readers)
     ;; Ensure the setuid suc binary exists
     (os/setuid (file-append suc "/bin/suc")
                #:target suc-exe-target-name
                #:mode #o554
                #:owner writers
                #:group writers))))

(define (os/git os)
    "Return a copy of OS (an operating-system) edited in such a way that a user
and group named git exist. Anybody can log is as git through SSH, with no
password or keys.  If everything works right, git should be quite
powerless (no shell, no port forward, etc.), and should only be able to be
used to access git repositories on the os through ssh."
    (-> os
        (packages git)
        (groups
         (user-group
          (name "git")
          (system? #t)))
        (users
         (user-account
          (name "git")
          (group "git")
          (home-directory "/srv/git/")
          (shell (file-append git "/bin/git-shell"))
          (system? #t)
          (password "")))
        (os/mkdir-p "/srv/git/"
                    #:mode #o570
                    #:owner "git"
                    #:group "users")
        (modify-service openssh
         (password-authentication? #f)
         (allow-empty-passwords? #t)
         (extra-content "
Match User git
    PasswordAuthentication yes
    AuthenticationMethods none
    PermitEmptyPasswords yes
    AllowAgentForwarding no
    AllowTcpForwarding no
    AllowStreamLocalForwarding no
    PermitOpen none
    PermitTunnel no
    PermitTTY no
    X11Forwarding no
    Subsystem sftp /dev/null
"))))

(define (os/hostname os name)
    "Return a copy of OS (an operating-system) edited in such a way that its
hostname is NAME."
    (operating-system
      (inherit os)
      (host-name name)))

(define (os/permaudit os)
   "Return a copy of OS (an operating-system) edited in such a way that the
permaudit tool is installed, which implies it is setuid root, and the sudoers
files is edited to let root impersonate any group."
   (operating-system
     (inherit
      (-> os
          (packages permaudit)
          (os/setuid (file-append permaudit "/bin/permaudit")
                     #:mode #o554
                     #:owner "root"
                     #:group "users")))
      (sudoers-file (plain-file "sudoers" "root ALL=(ALL:ALL) ALL"))))

(define (os/9mount os)
   "Return a copy of OS (an operating-system) edited in such a way that the
9mount tool suite is installed, which implies it is setuid root."
   (-> os
       (packages 9mount)
       (os/setuid (file-append 9mount "/bin/9mount")
                  #:mode #o555
                  #:owner "root"
                  #:group "users")
       (os/setuid (file-append 9mount "/bin/9bind")
                  #:mode #o555
                  #:owner "root"
                  #:group "users")
       (os/setuid (file-append 9mount "/bin/9umount")
                  #:mode #o555
                  #:owner "root"
                  #:group "users")))

(define (os/mount.9p os)
   "Return a copy of OS (an operating-system) edited in such a way that the
mount.9p wrapper is installed, which implies it is setuid root."
   (operating-system
     (inherit
      (-> os
          (packages mount.9p)
          (os/setuid (file-append mount.9p "/bin/mount.9p")
                     #:mode #o555
                     #:owner "root"
                     #:group "users")))))

(define* (join-the-tilde-club os #:key
                              (users #f)
                              (group #f)
                              (domain "the-dam.org"))
  "Create, for each of the given users (or all users in the given group), a
~user dir in /srv/DOMAIN/, with the correct permission for them to share stuff
with the world in it.

It is assumed that a http(s)-static-content already exists that serves
/srv/DOMAIN to the world."
  (let ([users (if users users (users-in-group os group))])
    (let rec ((u users)
              (os os))
      (if (null? u)
          ;; End of recursion, return os
          os
          ;; Create the user's dir and carry on
          (rec (cdr u)
               (os/mkdir-p os
                           (string-append "/srv/" domain "/~" (car u))
                           #:mode #o755 #:owner (car u) #:group "nginx"))))))

(define (create-or-chmod-listen-scripts-and-dirs os mappings streak-length)
  "Extend the activation service in the operating system configuration OS by
creating or modifying the:
  - listen scripts in the /srv/listen directory,
  - the runtime dirs in /run/listen,
  - the log dirs in /var/log/listen.

Each script or dir corresponds to a TCP port from tcp0 to tcp65535.

  The function takes three parameters:
  - os: The operating system configuration object,
  - mappings: A list of user-to-port mappings. Each mapping is a pair
    where the first element is a username, and the second element is
    the starting port number of the range attributed to the user in the
    first element,
  - streak-length: the number of block to try and allocate each user in the
    mappings

  For example, with a big enough streak-length, a mapping of ((\"root\"
0) (\"alice\" 2048) (\"root\" 10000)) would mean that ports 2048-9999
inclusive belong to alice, and all the others belong to root.

  Experience shows that it is unwise to create 65536 directories and files at
each reboot or reconfigure. The STREAK-LENGTH argument can be used to reduce
that number. For example, with a STREAK-LENGTH of 10, the following example
would give ports 0-9 to root, 2048-2057 to alice, and 10000-10009 to root.

  The other ports would be available to root as only root can create files in
the /src/listen directory.

  If a listen script or dir already exists, it is chowned without being
recreated, preserving its contents. The function ensures that each TCP port
has a corresponding listen script with the correct ownership and permissions,
as defined by the provided mappings."
  (extend-service
     os
     activation
     (with-imported-modules '((guix build utils)
                              (ice-9 match))
       #~(begin
           (use-modules (guix build utils)
                        (ice-9 match))
           (define (expand-mappings mappings max-streak-length)
  "Generate a detailed list of port-to-user mappings, possibly covering the entire range
from tcp0 to tcp65535, in blocks of MAX-STREAK-LENGTH.

For performance reasons, it may not be desirable to create all 65536 tcpXXX
files and directories. By setting MAX-STREAK-LENGTH to a small number, blocks of
that length are created at each mapping boundary given in MAPPINGS."
             (let rec ((m (sort mappings (lambda (a b) (< (cadr a) (cadr b)))))
                       ;; The recursive function below assumes the ports are strictly increasing
                       (current-user "root")
                       (current-index 0)
                       (current-streak-length 0)
                       (current-expanded-mappings '()))
               (cond
                ;; End of recursion, return the result
                ((= current-index 65536) (reverse current-expanded-mappings))
                ;; Most common recursion case:
                ;; increase the index and the current streak length,
                ;; and if this streak length is below the maximum,
                ;; add the current mapping to the extended mappings.
                ((or
                  (null? m)
                  (< current-index (cadar m)))
                 (rec m current-user (+ 1 current-index) (+ 1 current-streak-length)
                      (if (< current-streak-length max-streak-length)
                          (cons (list current-user current-index) current-expanded-mappings)
                          current-expanded-mappings)))
                ;; Reached a boundary: remove the pair from m, change the current-user,
                ;; reset the streak-length to 0,
                ;; and call oneself again on the same index, and mappings
                ((= current-index (cadar m))
                 (rec (cdr m) (caar m) current-index 0 current-expanded-mappings)))))

           (for-each (match-lambda
                      ((user port)
                       (let ((script-path (string-append "/srv/listen/tcp"     (number->string port)))
                             (ns-path     (string-append "/srv/listen/tcp"     (number->string port) ".namespace"))
                             (run-path    (string-append "/run/listen/tcp"     (number->string port)))
                             (log-path    (string-append "/var/log/listen/tcp" (number->string port)))
                             (user-id    (vector-ref (getpw user)     2))
                             (listen-gid (vector-ref (getgr "listen") 2)))
                         (when (= 0 (modulo port 10000))
                           (format #t "Creating scripts and dirs for port ~a~%" port))
                         (unless (stat script-path #f)
                           (mknod script-path 'regular #o640 0)
                           (chown script-path user-id listen-gid))
                         (unless (stat ns-path #f)
                           (mknod ns-path 'regular #o640 0)
                           (chown ns-path user-id listen-gid))
                         (mkdir-p run-path)
                         (chown run-path user-id listen-gid)
                         (chmod run-path #o770)
                         (mkdir-p log-path)
                         (chown log-path user-id listen-gid)
                         (chmod log-path #o570))))
                     (expand-mappings (quote #$mappings) #$streak-length))))))

(define (username->u16 username)
  "Return a 16 bit unsigned integer from the last two bytes of the sha256 of
username."
  (let* ((hash (sha256 (string->utf8 username)))
         (hash-length (bytevector-length hash)))
    (bytevector-u16-ref hash (- hash-length 2) 'big)))

(define (share-ports-fairly user-list)
  "Return a port mapping, attributing the start port for each username according
to its hash, constrained in the range 2048-49151.

The mapping is a list of pairs, where each pair consists of a username and the
corresponding starting port number.

There is a bias towards the first 18431 (or 18432, I'm too lazy to work it out
properly) ports of the range, because we use the modulo to convert a 16bit
random number (the hash), into a number from 0 to 47104."
  (let* ((port-range-start 2048)
         (port-range-end 49151)  ; inclusive
         (range-size (+ (- port-range-end port-range-start) 1)))

    (define (username->port username)
      (+ port-range-start (modulo (username->u16 username) range-size)))

    (map (lambda (user) (list user (username->port user))) user-list)))

(define (group-name->gid os group-name)
  "Return the GID for a given GROUP-NAME in the operating-system configuration
OS."
  (let ((groups (operating-system-groups os)))
    (let loop ((groups groups))
      (if (null? groups)
          #f  ; Group not found
          (let ((group (car groups)))
            (if (string=? (user-group-name group) group-name)
                (user-group-id group)
                (loop (cdr groups))))))))

(define (users-in-group os group-name)
  "Return a list of usernames from the operating-system configuration OS who
belong to the group GROUP-NAME."
  (let* ((gid (group-name->gid os group-name))
         (users (operating-system-users os)))
    ;(format #t "Users in group ~a (~a) among ~a ~%" group-name gid users)
    (map user-account-name
         (filter (lambda (user)
                   (let ((group (user-account-group user))
                         (groups (user-account-supplementary-groups user)))
                     (or
                      (and (number? group) (number? gid) (= group gid))
                      (and (string? group) (string= group group-name))
                      (member group-name groups))))
                 users))))

(define* (nginx-to-listen-reverse-proxy os mappings #:key (toplevel "the-dam.org"))
  "Extend the given os' nginx configuration to redirect from subdomains to
portnumbers, according to mappings.

 mappings is a list of user-to-port mappings. Each mapping is a pair where the
first element is a username, and the second element is the starting port
number of the range attributed to the user in the first element.

This function calls https-reverse-proxy under the hood, without passing any
arguments about ssl files' location. They will therefore be determined by the
%cert-* paramaters, or fall back to default values."
  (let rec ((m mappings)
            (os os))
    (if (null? m)
        ;; End of recursion, return os
        os
        ;; Add an nginx redirection and carry on
        (rec (cdr m)
             (https-reverse-proxy
              os
              #:from-host (string-append (caar m) "." toplevel)
              #:to-port (cadar m))))))


(define (create-service-aliases os)
  "Extend the activation service in the operating system configuration OS by
creating aliases between service names and tcpXXXX scripts in /srv/listen.

For example, /srv/listen/finger will link to /srv/listen/tcp79.

The mapping between port numbers and protocol names are taken from net-base's
/etc/services file, itself derived from IANA.

Only the mappings for ports below 1024 are considered."
  (extend-service
     os
     activation
     (with-imported-modules '((guix build utils)
                              (ice-9 rdelim)
                              (ice-9 textual-ports)
                              (srfi srfi-34))
       #~(begin
           (use-modules (guix build utils)
                        (ice-9 rdelim)
                        (ice-9 textual-ports)
                        (srfi srfi-34))
           (define (read-lines port)
             "Why is this not part of some standard library ? Scheme is cool, but
I wish it included more batteries."
             (let loop ((line (read-line port))
                        (lines '()))
               (if (eof-object? line)
                   (reverse lines)
                   (loop (read-line port) (cons line lines)))))
           (let* ((etc/services #$(file-append net-base "/etc/services"))
                  (lines (call-with-input-file etc/services read-lines)))
             (for-each
              (lambda (line)
                (guard (ex (else #f))
                  ;; Exception handler does nothing, the majority of lines will generate an error
                  (let* ((trimmed-line (string-trim line))
                         (parts (string-tokenize trimmed-line))
                         (service-name (car parts))
                         (port-protocol (cadr parts))
                         (port (string->number (car (string-split port-protocol #\/)))))
                    (when (< port 1024)
                      (let ((target (string-append "/srv/listen/tcp" (number->string port)))
                            (link-name (string-append "/srv/listen/" service-name)))
                        ;;(format #t "Linking ~a -> ~a ~%" link-name target)
                        (symlink target link-name))
                      (let ((target (string-append "/srv/listen/tcp" (number->string port) ".namespace"))
                            (link-name (string-append "/srv/listen/" service-name ".namespace")))
                        ;;(format #t "Linking ~a -> ~a ~%" link-name target)
                        (symlink target link-name))
                      (let ((target (string-append "/run/listen/tcp" (number->string port)))
                            (link-name (string-append "/run/listen/" service-name)))
                        ;;(format #t "Linking ~a -> ~a ~%" link-name target)
                        (symlink target link-name))
                      (let ((target (string-append "/var/log/listen/tcp" (number->string port)))
                            (link-name (string-append "/var/log/listen/" service-name)))
                        ;;(format #t "Linking ~a -> ~a ~%" link-name target)
                        (symlink target link-name))))))
              lines))))))

(define (listen-requirements->shepherd-service requirements)
  "Return a sherpherd service from the list of requirements that makes up the
listen-requirements-service-type configuration"
  (list
   (shepherd-service
    (documentation "Dummy service collecting listen's requirements")
    (requirement requirements)
    (provision '(listen-services))
    (one-shot? #t)
    (start #~(lambda _ #t))
    (stop  #~(lambda _ #t)))))

(define listen-requirements-service-type
  (service-type
   (name 'listen-requirements)
   (description "Collect the shepherd requirements of listen")
   (extensions
    (list
     (service-extension shepherd-root-service-type listen-requirements->shepherd-service)))
   (default-value '())))

(define (register-listen-service os service-name)
  "Register SERVICE-NAME as a requirement to be started before listen can start.

Typically useful for making sure that 9P servers and guix profiles are there
before listen services need them."
   (operating-system
     (inherit os)
     (services
      (modify-services (operating-system-user-services os)
        (listen-requirements-service-type config => (cons service-name config))))))

(define (listen-shepherd os)
  "Return a copy of OS, adding a shepherd service that launches the listen script at boot.

The script ought to be run in a container, but complex interaction between
capabilities and container prevented me from achieving that goal, so the
script is just run normally. (FIXME)"
  (->
   os
   (extend-service
    shepherd-root
    (list
     (shepherd-service
      (documentation "Run the listen daemon, rev 3")
      (requirement '(guix-daemon ;; Needed because listen calls guix
                     networking ;; listen listens on ports
                     listen-services))  ;; Dummy service that must
      ;; be started before listen can
      ;; start
      (provision '(listen))
      (start #~(make-forkexec-constructor
                (list #$(file-append listen "/bin/listen"))
                #:user "listen"
                #:group "listen"
                #:environment-variables '("HOME=/run/listen")
                #:pid-file "/run/listen/listen.pid"
                #:log-file "/var/log/listen/listen.log"))
      (stop #~(make-kill-destructor)))))
   (add-service listen-requirements)))

(define* (os/listen os #:key (domain "the-dam.org"))
  "Return a copy of OS (an operating-system) edited in such a way that the listen
daemon starts at boot, monitoring the /srv/listen/ directory for service scripts to launch."
  (-> os
      (packages listen go-github-com-hugelgupf-p9-cmd-p9ufs the-dam-org-f29p)
      (groups "listen")
      (nobody-like-user "listen" #:home "/run/listen/")
      (os/mkdir-p "/srv/9p"         #:mode #o1777 #:owner "root"   #:group "root")  ;; Just in case it's not already there
      (os/mkdir-p "/srv/listen"     #:mode #o755  #:owner "root"   #:group "root")
      (os/mkdir-p "/var/log/listen" #:mode #o755  #:owner "listen" #:group "users")
      (os/mkdir-p "/run/listen"     #:mode #o755  #:owner "listen" #:group "users")
      (extend-service
       activation
        #~(begin
            (unless (stat "/srv/listen/listen.namespace" #f)
              (symlink #$(file-append listen "/bin/listen.namespace")
                       "/srv/listen/listen.namespace"))))
      (os/profile "/run/listen/profile"
                  #:name 'default-listen-profile
                  #:packages '("guix" "util-linux-with-udev" "bash"
                               "coreutils" "socat"))
      (extend-service boot  ;; Cleanup /run/listen at boot time (which is
                            ;; before activation time when it is created)
                      (with-imported-modules '((guix build utils))
                        #~(when (stat "/run/listen" #f)
                            (delete-file-recursively "/run/listen"))))
      (create-or-chmod-listen-scripts-and-dirs
       (append
        (share-ports-fairly (users-in-group os "users"))
        (list '("root" 49152))) 12)
      (modify-service
       sysctl
       config => (settings (append
                            (sysctl-configuration-settings config)
                            ;; Moving the ephemeral ports range away from its default 32768-60999
                            '(("net.ipv4.ip_local_port_range" . "49152 65535")
                              ;; Privilege ALL THE PORTS
                              ("net.ipv4.ip_unprivileged_port_start" . "49152")))))
      ;; Grant privileged-port-binding capabilities to a wrapper owned and
      ;; executable only by the listen user
      (os/setcap (file-append listen "/bin/with-cap-bind")
                 "cap_net_bind_service=+p"
                 #:mode #o500
                 #:owner "listen"
                 #:group "listen")
      ;; Redirect from https://user.domain to the first port of the user's range
      (nginx-to-listen-reverse-proxy
       (share-ports-fairly (users-in-group os "users"))
       #:toplevel domain)
      (create-service-aliases)
      ;; Shepherd service
      (listen-shepherd)
      (register-listen-service 'default-listen-profile)))

(define* (os/shepherd-service os cmd cwd #:key (name #f))
  "Return a copy of OS, adding a shepherd service that launches CMD in CWD.

CMD is a shell line, given to bash -c.

The user/group under which CMD is run is the owner of CWD."
  (let [(name (or name (car (string-split cmd #\ ))))]
    (extend-service
     os
     shepherd-root
     (list (shepherd-service
            (documentation (string-append "Run the " name " daemon"))
            (requirement '(user-homes))
            (provision (list (string->symbol name)))
            (start #~(make-forkexec-constructor
                      (list #$(file-append
                               bash
                               "/bin/bash") "-c" #$(string-append
                                                    "cd " cwd "; "
                                                    "source /run/current-system/profile/etc/profile; "
                                                    "sudo -u " user " " cmd))
                      ;; FIXME: Using the #:directory keyword argument to
                      ;; make-forkexec-constructor yields the following error:
                      ;; ice-9/boot-9.scm:1683:16: In procedure raise-exception:
                      ;; No applicable method for #<<generic> one-shot-service? (1)> in call (one-shot-service? #f)
                      ;; Until this bug is fixed, we eschew its usage here and instead "cd" our way
                      ;; to the correct directory in the command above.
                      ;; Once the bug is solved in mainstream, do it properly here.
                      ;; #:directory cwd
                      #:user (passwd:name (getpw    (stat:uid (stat #$cwd))))
                      #:group (group:name (getgrgid (stat:gid (stat #$cwd))))
                      #:log-file #$(string-append "/var/log/" name ".log")))
            (stop #~(make-kill-destructor)))))))

(define (listen/echo os)
  "Return a copy of OS, in which listen's echo (tcp7) service is active."
  (extend-service
     os
     activation
     #~(begin
         (when (file-exists? "/srv/listen/tcp7")
           (delete-file "/srv/listen/tcp7"))
         (symlink #$(file-append coreutils "/bin/cat") "/srv/listen/tcp7"))))

(define* (os/profile os profile #:key (packages '())
                     (name #f) (user "root") (group "root"))
  "Return a copy of OS (an operating-system) edited in such a way that the given
PACKAGES are available in the profile at PROFILE."
  (let [(name (or name
                  (string->symbol (string-append "profile:" (basename profile)))))]
    (extend-service
     os
     shepherd-root
     (list (shepherd-service
            (documentation (string-append "Create profile: " profile))
            (requirement '(user-homes networking guix-daemon))
            (provision (list name))
            (one-shot? #t)
            (start #~(lambda _
                             (invoke "/root/.config/guix/current/bin/guix"
                                     "install" (string-append "--profile=" #$profile)
                                     #$@packages))))))))

(define* (os/9p-serve os dir _socket #:key
                      (user "nobody") (read-write "0")
                      (owner "nobody") (group "nogroup") (mode "700")
                      (name #f))
  "Return a copy of OS, adding a shepherd service that runs a 9P server
serving DIR on _SOCKET.

Access control works by first choosing the least privileged USER possible to
run the service, setting READ-WRITE to #t only if necessary, and choosing the
appropriate OWNER, GROUP and MODE for the socket.

There is no 9P authentication. Users able to rw the socket will be able to act
on behalf of USER via the 9P server."
  (->
   os
   (os/mkdir-p dir)
   (os/mkdir-p (dirname _socket))
   (packages go-github-com-hugelgupf-p9-cmd-p9ufs lsof coreutils)
   (extend-service
    shepherd-root
    (list (shepherd-service
           (documentation (string-append "Serve " dir " on " _socket))
            (requirement '(user-homes))
            (provision (list name))
            (start #~(make-forkexec-constructor
                      (list
                       #$(file-append bash "/bin/bash")
                       #$(plain-file "9pserve.sh" "
# It is kind of odd that we have to source the system's profile
# I would have expected it to be the default profile for shepherd services
. /run/current-system/profile/etc/profile
set -euxo pipefail
dir=\"$1\"
socket=\"$2\"
user=\"$3\"
read_write=\"$4\"
owner=\"$5\"
group=\"$6\"
mode=\"$7\"

cd \"$dir\"
if [ -S \"$socket\" ]
then
    if ! lsof \"$socket\" > /dev/null
    then
        # Socket exists but nobody is listening on it: remove it
        rm -f \"$socket\"
    else
        echo Socket $socket exists and somebody is listening on it. Killing myself
        exit 1
    fi
fi
# At this point the socket does not exist, unless there is a race condition
if [ \"$read_write\" == \"0\" ]
then
    container_arg=\"--expose=$dir\"
else
    container_arg=\"--share=$dir\"
fi
sudo -u \"$user\" guix shell --container \"$container_arg\" \
    --share=\"$(dirname \"$socket\")\" \
    --profile=/run/current-system/profile -- \
    p9ufs --root=\"$dir\" -v --unix \"$socket\" &
p9ufs_pid=$!
cleanup () {
     kill -9 \"$p9ufs_pid\"
}
trap cleanup EXIT
sleep 1;
if ! [ -S \"$socket\" ];
then
   sleep 10;
   if ! [ -S \"$socket\" ]
   then
      echo The socket $socket did not appear, killing myself
      exit 1
   fi
fi
chown \"$owner:$group\" \"$socket\"
chmod \"$mode\" \"$socket\"
echo $p9ufs_pid > \"$socket\".pid
wait
")
                       #$dir
                       #$_socket
                       #$user
                       #$read-write
                       #$owner
                       #$group
                       #$mode
                       )
                      #:pid-file #$(string-append _socket ".pid")
                      #:pid-file-timeout 15  ;; The default is too short, the
                                             ;; service is killed during the
                                             ;; 'sleep 10'.
                      #:group #$group
                      #:log-file #$(string-append "/var/log/" (symbol->string name) ".log")
                      ))
            (stop #~(make-kill-destructor)))))))

(define (listen/finger os)
  "Return a copy of OS, in which listen's finger (tcp79) service is active."
  (->
   os
   (groups "finger")
   (nobody-like-user "finger" #:home "/run/listen/tcp79")
   (os/mkdir-p "/srv/finger"           #:mode #o1775 #:owner "finger"   #:group "users")
   (os/mkdir-p "/var/log/listen/tcp79" #:mode #o755  #:owner "listen"   #:group "users")
   (os/mkdir-p "/run/listen/tcp79"     #:mode #o1775 #:owner "listen"   #:group "users")
   (os/9p-serve "/srv/finger/" "/srv/9p/finger" #:name '9p-finger
                #:user "listen" #:owner "listen" #:group "listen" #:mode "700")
   (register-listen-service '9p-finger)
   (extend-service
    activation
    #~(begin
        (unless (file-exists? "/srv/listen/tcp79")
          (symlink #$(file-append fingerd "/bin/fingerd/tcp79")           "/srv/listen/tcp79"))
        (unless (file-exists? "/srv/listen/tcp79.namespace")
          (symlink #$(file-append fingerd "/bin/fingerd/tcp79.namespace") "/srv/listen/tcp79.namespace"))))
   (os/profile "/run/listen/tcp79/profile"
               #:name 'finger-profile
               #:packages '("python" "the-dam-org-f29p" "fuse"))
   (register-listen-service 'finger-profile)))

(define (listen/finger/hello os)
  "Return a copy of OS, in which listen's finger (tcp79)'s hello endpoint is
active."
   (extend-service
    os
    activation
    #~(begin
        (when (file-exists? "/srv/finger/hello")
          (delete-file "/srv/finger/hello"))
        (symlink #$(file-append fingerd "/bin/fingerd/hello") "/srv/finger/hello"))))

(define (listen/finger/inspect os)
  "Return a copy of OS, in which listen's finger (tcp79)'s inspect endpoint is active."
   (extend-service
    os
    activation
    #~(begin
        (when (file-exists? "/srv/finger/inspect")
          (delete-file "/srv/finger/inspect"))
        (symlink #$(file-append fingerd "/bin/fingerd/inspect") "/srv/finger/inspect"))))

(define (listen/finger/snail os)
  "Return a copy of OS, in which listen's finger (tcp79)'s snail endpoint is active."
  (->
   os
   (extend-service
    activation
    #~(begin
        (when (file-exists? "/srv/finger/snail")
          (delete-file "/srv/finger/snail"))
        (symlink #$(file-append fingerd "/bin/fingerd/snail") "/srv/finger/snail")))
   (os/profile "/run/listen/tcp79/snail.profile"
               #:name 'snail-profile
               #:packages '("pv" "asp"))
   (register-listen-service 'snail-profile)))

(define* (listen/finger/user-default os #:key
                                     (users #f)
                                     (group #f))
  "Create, for each of the given users (or all users in the given group), a
default USER script in /srv/finger/. If something already exists, it is not overwritten."
  (let ([users (if users users (users-in-group os group))])
    (let rec ((u users)
              (os os))
      (if (null? u)
          ;; End of recursion, return os
          os
          ;; Create the user's script and carry on
          (rec (cdr u)
               (extend-service
                os
                activation
                #~(unless (file-exists? (string-append "/srv/finger/" #$(car u)))
                    (symlink #$(file-append fingerd "/bin/fingerd/default_script")
                             (string-append "/srv/finger/" #$(car u)))
                    (invoke #$(file-append coreutils "/bin/chown")
                            "-h" ;; We chown the link itself, not the target
                            (string-append #$(car u) ":users")
                            (string-append "/srv/finger/" #$(car u))))))))))

(define (listen/git-daemon os)
  ;; As a workaround for this bug: https://issues.guix.gnu.org/64648 we
  ;; provide anonymous git:// access as well, for all repos in /srv/git.
  ;; This is far from ideal. I would like to use SSH only as provided in os/git.
  "Return a copy of OS, in which listen's git daemon (tcp9418) service is active."
  (->
   os
   ;; Assume user and group git were created elsewhere, typically with os/git.
   ;; Same for /srv/git.
    (os/mkdir-p "/var/log/listen/tcp9418" #:mode #o755  #:owner "listen"   #:group "git")
    (os/mkdir-p "/run/listen/tcp9418"     #:mode #o1770 #:owner "listen"   #:group "git")
    (os/9p-serve "/srv/git/" "/srv/9p/git" #:name '9p-git
                 #:user "git" #:owner "listen" #:group "listen" #:mode "700")
    (register-listen-service '9p-git)
    (os/profile "/run/listen/tcp9418/profile"
                #:name 'git-profile
                #:packages '("git" "iproute2" "coreutils" "bash" "socat"
                             "the-dam-org-f29p" "fuse"))
    (register-listen-service 'git-profile)
    (extend-service
     activation
     #~(begin
         (when (file-exists? "/srv/listen/tcp9418")
           (delete-file "/srv/listen/tcp9418"))
         (symlink #$(file-append coreutils "/bin/true")           "/srv/listen/tcp9418")
         (when (file-exists? "/srv/listen/tcp9418.namespace")
           (delete-file "/srv/listen/tcp9418.namespace"))
         (copy-file
          #$(plain-file "git-daemon.sh"
                        "#!/usr/bin/env bash
set -euxo pipefail
mkdir -p /srv/git
f29p unix!/srv/9p/git /srv/git &
ip link set lo up
git daemon --base-path=/srv/git --export-all&
exec socat -dd UNIX-listen:/run/socket,fork TCP-CONNECT:127.0.0.1:9418
")
          "/srv/listen/tcp9418.namespace")
         (chown "/srv/listen/tcp9418.namespace"
                (vector-ref (getpw "git") 2)
                (vector-ref (getgr "listen") 2))
         (chmod "/srv/listen/tcp9418.namespace" #o775)))))
