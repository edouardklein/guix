(define-module (beaver packages ricing)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system ruby)
  #:use-module (guix git)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages compton)
  )

(define-public picom-ftlabs
  (let ((revision "0")
        (commit "df4c6a3d9b11e14ed7f3142540babea4c775ddb1"))
    (package
      (inherit picom)
      (name "picom-ftlabs")
      (version (git-version "20240519" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/yshui/picom")
               (commit commit)))
         (sha256
          (base32
           "0y89shsmw0qvxk4qmxss82hr2zibmk6hncnlkb05jb6jiv2r2qqn"))
         (file-name (git-file-name name version)))))))

(define-public picom-next
  (let ((revision "1")
        (commit "9a83982d7ef575b47df71bdc895d7a264f33c392"))
    (package
      (inherit picom)
      (name "picom-next")
      (version (git-version "20240519" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/yshui/picom")
               (commit commit)))
         (sha256
          (base32
           "0wfi4nav2llprwxlgc4fzrj25hk9rdg3kychbq45zhhgljjpq2sf"))
         (file-name (git-file-name name version)))))))

(define-public picom-next
  (let ((revision "1")
        (commit "9a83982d7ef575b47df71bdc895d7a264f33c392"))
    (package
      (inherit picom)
      (name "picom-next")
      (version (git-version "20240519" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/yshui/picom")
               (commit commit)))
         (sha256
          (base32
           "0wfi4nav2llprwxlgc4fzrj25hk9rdg3kychbq45zhhgljjpq2sf"))
         (file-name (git-file-name name version)))))))

(define-public picom-jonaburg
  (let ((revision "0")
        (commit "e3c19cd7d1108d114552267f302548c113278d45"))
    (package
      (inherit picom)
      (name "picom-jonaburg")
      (version (git-version "20240518" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/jonaburg/picom")
               (commit commit)))
         (sha256
          (base32
           "19nglw72mxbr47h1nva9fabzjv51s4fy6s1j893k4zvlhw0h5yp2"))
         (file-name (git-file-name name version))))
      (inputs
       (list dbus
             libconfig
             libepoxy
             libev
             libx11
             libxext
             libxdg-basedir
             mesa
             pcre
             pixman
             uthash
             xcb-util
             xcb-util-renderutil
             xcb-util-image
             xprop))
      (native-inputs
       (list asciidoc pkg-config xorgproto))
      )))

(define-public edk-dwm
  (package (inherit dwm)
	   (name "edk-dwm")
	   (version "9a57caa59816c470451bc1b5985295d948806284")
	   (source
	    (origin
	      (method git-fetch)
	      (uri (git-reference
		    (url "https://gitlab.com/edouardklein/dwm")
		    (commit version)))
	      (file-name (git-file-name name version))
	      (sha256 
	       "0hmdpfpc09q9rqqnzhl4f56y0ry8ssmiz89wrw1xm3fpvlalhf67")))))


