(define-module (beaver packages python-xyz)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (guix modules)
  #:use-module (gnu packages)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system shadow)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix profiles)
  #:use-module (guix records)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages dbm)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages base)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages autotools)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (requisomatic-webapp))

(define-public python-ipaddr
  (package
    (name "python-ipaddr")
    (version "2.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ipaddr" version))
        (sha256
          (base32
            "1ml8r8z3f0mnn381qs1snbffa920i9ycp6mm2am1d3aqczkdz4j0"))))
    (build-system python-build-system)
    (home-page "https://github.com/google/ipaddr-py")
    (synopsis
      "Google's IP address manipulation library")
    (description
      "Google's IP address manipulation library")
    (license license:asl2.0)))

(define-public python-ipwhois
  (package
    (name "python-ipwhois")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ipwhois" version))
        (sha256
          (base32
            "1gjbr2bbarhz47nk8kqpnz4cci8gp3imd54f0hd20hc07gpky7l3"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-dnspython" ,python-dnspython-1.16)
        ("python-ipaddr" ,python-ipaddr)))
    (home-page "https://github.com/secynic/ipwhois")
    (synopsis
      "Retrieve and parse whois data for IPv4 and IPv6 addresses.")
    (description
      "Retrieve and parse whois data for IPv4 and IPv6 addresses.")
    (license license:bsd-3)))

(define-public python-markdown2
  (package
    (name "python-markdown2")
    (version "2.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "markdown2" version))
        (sha256
          (base32
            "06wvhai41in8xdvwmn97d6d0fyvlhsqyj0agd27zdrj4wpq6kmr8"))))
    (build-system python-build-system)
    (home-page
      "https://github.com/trentm/python-markdown2")
    (synopsis
      "A fast and complete Python implementation of Markdown")
    (description
      "A fast and complete Python implementation of Markdown")
    (license license:expat)))

(define-public python-secretary
  (package
    (name "python-secretary")
    (version "ebaa46b1378a21af6cfd914a1804b359ddd9a3e5")
    (source
      (origin
        (method git-fetch)
      (uri (git-reference
            (url "https://github.com/Jenselme/secretary")
            ;; Fork of the official https://github.com/christopher-ramirez/secretary
            ;; The fork works with the new version of jinja
            (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "1bdp70kkc9ihc5hzn1nvqcdrfx5rvwq3c1sj49b2r4djc1arpy3i"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-jinja2" ,python-jinja2)
        ("python-markdown2" ,python-markdown2)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'check))))
    (home-page
      "https://github.com/christopher-ramirez/secretary")
    (synopsis
      "Take the power of Jinja2 templates to OpenOffice or LibreOffice.")
    (description
      "Take the power of Jinja2 templates to OpenOffice or LibreOffice.")
    (license license:expat)))

(define *requisomatic-version* "1.0.3")

(define-public requisomatic
  (package
   (name "requisomatic")
   (version *requisomatic-version*)
   (source
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://gitlab.com/edouardklein/requisomatic")
             (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "1krwhvpxcbq9xyhphb9pfn6w6qkkkrsxlnlpblvnmqcn0ql6sczp"))))
   (propagated-inputs `(("python" ,python)
                        ("python-flask" ,python-flask)
                        ("python-ipwhois" ,python-ipwhois)
                        ("python-pandas" ,python-pandas)
                        ("python-pygments" ,python-pygments)
                        ("python-secretary" ,python-secretary)
                        ("curl" ,curl)))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan '(("./" "bin/requisomatic"))
      #:phases
      (modify-phases %standard-phases
        (add-before
            'build 'check
          (lambda* (#:key outputs #:allow-other-keys)
            (let* ((dir (string-append
                         (assoc-ref outputs "out")
                         "/bin/requisomatic")))
              (chdir dir)
              (invoke "make" "test")
              #t))))))
   (synopsis "Quickly edit multiple subpoenas")
   (description
    "Give IP, get supboenas.")
   (home-page "https://gitlab.com/edouardklein/requisomatic")
   (license license:agpl3+)))

(define serviceable-requisomatic
  (package
   (name "serviceable-requisomatic")
   (version *requisomatic-version*)
   (source #f)
   (propagated-inputs
    `(("requisomatic" ,requisomatic)
      ("gunicorn" ,gunicorn)
      ("bash" ,bash)))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((bash (assoc-ref %build-inputs "bash"))
               (requisomatic (assoc-ref %build-inputs "requisomatic"))
               (dir (string-append (assoc-ref %outputs "out") "/bin"))
               (fname (string-append dir "/requisomatic-server")))
          (mkdir-p  dir)
          (with-output-to-file fname
            (lambda _
              (display (string-append "#!" bash "/bin/bash\n"))
              (display "source /run/current-system/profile/etc/profile\n")
              (display "REQUISOMATIC_DB_FILE=$1 gunicorn --bind=$2 --pid=$3 requisomatic:app\n")))
          (chmod fname #o755)
          #t))))
   (synopsis "A custom package to run requisomatic as a server")
   (description #f)
   (license #f)
   (home-page #f)))

(define-record-type* <requisomatic-configuration>
  requisomatic-configuration make-requisomatic-configuration
  requisomatic-configuation?
  (user          requisomatic-configuration-user
                 (default "requisomatic"))         ;string
  (group         requisomatic-configuration-group         ;string
                 (default "requisomatic"))
  (db-file       requisomatic-db-file   ; path
                 (default "/var/lib/requisomatic/db.sqlite3"))
  (log-file      requisomatic-log-file ;path
                 (default "/var/log/requisomatic.log"))
  (bind-to       requisomatic-bind-to  ; https://docs.gunicorn.org/en/stable/run.html#commonly-used-arguments
                 ;; see the --bind option of gunicorn
                 (default "127.0.0.1:8000"))
  (pid-file      requisomatic-pid-file ;path
                 (default "/var/run/requisomatic/gunicorn.pid")))

(define requisomatic-shepherd
  (match-lambda
   (($ <requisomatic-configuration> user group db-file log-file bind-to pid-file)
    (with-imported-modules (source-module-closure
                            '((guix build utils)
                              (gnu build shepherd)
                              (gnu system file-systems)))
      (list (shepherd-service
             (provision '(requisomatic))
             (requirement '(user-processes networking))
             (documentation (string-append "Run the requisomatic web server with gunicorn"))
             (auto-start? #t)
             (start #~(begin
                        (format #t "coucou************************************\n")
                        (use-modules (guix build utils)
                                     (gnu build shepherd)
                                     (gnu system file-systems))
                        (make-forkexec-constructor
                         ;(make-forkexec-constructor/container ;; I can't make this work, the mappings below don't work, so fuckit for now
                         '(#$(file-append serviceable-requisomatic "/bin/requisomatic-server") #$db-file #$bind-to #$pid-file)
                         #:directory (string-append #$requisomatic "/bin/requisomatic/")
                         #:pid-file #$pid-file
                         #:log-file #$log-file
                         #:user #$user
                         #:group #$group
                         ;; #:mappings `(
                         ;;              (file-system-mapping
                         ;;               (source ,(dirname #$pid-file))
                         ;;               (target source)
                         ;;               (writable? #t))
                         ;;              (file-system-mapping
                         ;;               (source ,(dirname #$log-file))
                         ;;               (target source)
                         ;;               (writable? #t))
                         ;;              (file-system-mapping
                         ;;               (source ,(dirname $db-file))
                         ;;               (target source)
                         ;;               (writable? #t))
                         ;;              (file-system-mapping
                         ;;               (source "/run/current-system/profile/")
                         ;;               (target source)
                         ;;               (writable? #t)))

                         )))
             (stop #~(make-kill-destructor))))))))

(define requisomatic-activation
  (match-lambda
   (($ <requisomatic-configuration> user group db-file log-file bind-to pid-file)
    (with-imported-modules '((guix build utils))
      #~(begin
           (use-modules (guix build utils))
           (let ((db-dir  (dirname #$db-file))
                 (pid-dir (dirname #$pid-file)))
             (mkdir-p db-dir)
             (chown db-dir (passwd:uid (getpwnam #$user)) (group:gid (getgrnam #$group)))
             (mkdir-p pid-dir)
             (chown pid-dir (passwd:uid (getpwnam #$user)) (group:gid (getgrnam #$group)))))))))

(define requisomatic-account
  (match-lambda
   (($ <requisomatic-configuration> user group db-file log-file bind-to pid-file)
    (list
     (user-group
      (name group))
     (user-account
      (name user)
      (group group))))))

(define requisomatic-service-type
  (service-type (name 'requisomatic)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          requisomatic-shepherd)
                       (service-extension activation-service-type
                                          requisomatic-activation)
                       (service-extension account-service-type
                                          requisomatic-account)
                       (service-extension profile-service-type
                                          (const (list serviceable-requisomatic)))))
                (default-value (requisomatic-configuration))
                (description "Run the requisomatic web application with gunicorn.")))

(define* (requisomatic-webapp os #:key
                                     (user "requisomatic")
                                     (group "requisomatic")
                                     (pid-file "/var/run/requisomatic/gunicorn.pid")
                                     (db-file "/var/lib/requisomatic/db.sqlite3")
                                     (port 8000)
                                     (ip "127.0.0.1")
                                     (log-file "/var/log/requisomatic.log"))
  (operating-system
    (inherit os)
    (services
     (cons (service requisomatic-service-type
                    (requisomatic-configuration
                     (user user)
                     (group group)
                     (db-file db-file)
                     (log-file log-file)
                     (pid-file pid-file)
                     (bind-to (format #f "~a:~a" ip port))))
           (operating-system-user-services os)))))

(define-public python-dice
  (package
    (name "python-dice")
    (version "3.1.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "dice" version))
              (sha256
               (base32
                "1x6lqlijdi65cg49dpplmiksn36v0a91c4bkhqqgc9vkzmy4gpz3"))))
    (build-system python-build-system)
    (propagated-inputs (list python-docopt python-pyparsing python-pytest))
    (home-page "https://github.com/borntyping/python-dice")
    (synopsis "A library for parsing and evaluating dice notation")
    (description
     "This package provides a library for parsing and evaluating dice notation")
    (license license:expat)))

(define-public python-coincurve
  (package
    (name "python-coincurve")
    (version "18.0.0")
    (source
     (origin
       ;; (method url-fetch)
       ;; (uri (pypi-uri "coincurve" version))
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ofek/coincurve")
             (commit "2dfea673835e524ffaeb11dc1c2aecaf23188195")))
       (sha256
        (base32 "0sclzqcjbx09asbmls7zxdq2yydyz3ycq4nab8v36wnvz1qvrx3a"))))
    (build-system python-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'setenv
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((libsecp256k1 (assoc-ref inputs "libsecp256k1")))
               ;; Make the build find our version of the lib instead of downloading it
               (setenv "LIB_DIR" (string-append libsecp256k1 "/lib"))
               ;; Trick install_egg_info into thinking the source code of the lib is there
               (mkdir-p "libsecp256k1/")
               (invoke "touch" "libsecp256k1/autogen.sh")
               #t)))
         (delete 'check))))
    (native-inputs (list autoconf automake libtool libsecp256k1 ))
    (propagated-inputs (list libsecp256k1 python-asn1crypto python-cffi python-requests))
    (home-page "")
    (synopsis "Cross-platform Python CFFI bindings for libsecp256k1")
    (description "Cross-platform Python CFFI bindings for libsecp256k1")
    (license #f)))

(define-public python-green
  (package
    (name "python-green")
    (version "3.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "green" version))
       (sha256
        (base32 "19k8fc97yy65702ny46alwnvbg425jnmh6k0vm8kicawgfvx0rc8"))))
    (build-system python-build-system)
    (propagated-inputs (list python-colorama python-coverage python-lxml
                             python-unidecode))
    (home-page "https://github.com/CleanCut/green")
    (synopsis "Green is a clean, colorful, fast python test runner.")
    (description "Green is a clean, colorful, fast python test runner.")
    (license license:expat)))

(define-public python-btcrecover
  (package
    (name "python-btcrecover")
    (version "1.12.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/3rdIteration/btcrecover")
             (commit "74fee3f0c38f8bd05e187012f51b1ad26ca9a145")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1av40kb5v9yrv7qm4wpa75v0bxwk86mscf02s07sakj7ma0x6kn8"))))
    (propagated-inputs (list python-coincurve python-green python-protobuf python-pycryptodome
                             python-requests python))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("./" "bin/btcrecover"))
       #:phases
       (modify-phases %standard-phases
         (add-before
             'build 'check
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((dir (string-append
                          (assoc-ref outputs "out")
                          "/bin/btcrecover")))
               (chdir dir)
               (setenv "HOME" (getcwd))
               (invoke "python3" "run-all-tests.py" "-vv")
               #t))))))
    (synopsis "Open source wallet password and seed recovery tool.")
   (description
    "For seed based recovery, this is primarily useful in situations where you have lost/forgotten parts of your mnemonic, or have made an error transcribing it. (So you are either seeing an empty wallet or gettign an error that your seed is invalid)

For wallet password or passphrase recovery, it is primarily useful if you have a reasonable idea about what your password might be.")
   (home-page "https://btcrecover.readthedocs.io")
   (license #f)))

(define-public python-bip32
  (package
    (name "python-bip32")
    (version "3.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/darosior/python-bip32")
             (commit "1492d39312f1d9630363c292f6ab8beb8ceb16dd")))
       (sha256
        (base32 "048zwh1i11jbs6h8j8sqbl7kx5hcyhr7i61mq5snl3fpbr3hmid3"))))
    (build-system python-build-system)
    (propagated-inputs (list python-base58 python-coincurve))
    (home-page "http://github.com/darosior/python-bip32")
    (synopsis "Minimalistic implementation of the BIP32 key derivation scheme")
    (description
     "Minimalistic implementation of the BIP32 key derivation scheme")
    (license license:expat)))


(define-public python-3.11
  (package
    (name "python-3.11")
    (version "3.11.10")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://www.python.org/ftp/python/" version
                           "/Python-" version ".tar.xz"))
       (sha256
        (base32 "0wp3psmpmxnhhi76pk9y44p021aadjh4j2fb2ldfc019j5p3b907"))
       (patches (search-patches "python-3-deterministic-build-info.patch"))
       (modules '((guix build utils)))
       (snippet '(begin
                   ;; Delete the bundled copy of libexpat.
                   (delete-file-recursively "Modules/expat")
                   (substitute* "Modules/Setup"
                     ;; Link Expat instead of embedding the bundled one.
                     (("^#pyexpat.*")
                      "pyexpat pyexpat.c -lexpat\n"))
                   ;; Delete windows binaries
                   (for-each delete-file
                             (find-files "Lib/distutils/command" "\\.exe$"))))))
    (outputs '("out" "tk" ;tkinter; adds 50 MiB to the closure
               "idle")) ;programming environment; weighs 5MB
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:configure-flags (list "--enable-shared" ;allow embedding
                               "--with-system-expat" ;for XML support
                               "--with-system-ffi" ;build ctypes
                               "--with-ensurepip=install" ;install pip and setuptools
                               "--with-computed-gotos" ;main interpreter loop optimization
                               "--enable-unicode=ucs4"
                               "--without-static-libpython"
                               "--enable-loadable-sqlite-extensions"

                               ;; FIXME: These flags makes Python significantly faster,
                               ;; but leads to non-reproducible binaries.
                               "--with-lto"   ;increase size by 20MB, but 15% speedup
                                "--enable-optimizations"

                               ;; Prevent the installed _sysconfigdata.py from retaining
                               ;; a reference to coreutils.
                               "INSTALL=install -c"
                               "MKDIR_P=mkdir -p"

                               ;; Disable runtime check failing if cross-compiling, see:
                               ;; https://lists.yoctoproject.org/pipermail/poky/2013-June/008997.html
                               ,@(if (%current-target-system)
                                     '("ac_cv_buggy_getaddrinfo=no"
                                       "ac_cv_file__dev_ptmx=no"
                                       "ac_cv_file__dev_ptc=no")
                                     '())
                               ;; -fno-semantic-interposition reinstates some
                               ;; optimizations by gcc leading to around 15% speedup.
                               ;; This is the default starting from python 3.10.
                               "CFLAGS=-fno-semantic-interposition"
                               (string-append "LDFLAGS=-Wl,-rpath="
                                              (assoc-ref %outputs "out")
                                              "/lib"
                                              " -fno-semantic-interposition"
                                              " -lffi"))
       #:modules ((ice-9 ftw)
                  (ice-9 match)
                  (guix build utils)
                  (guix build gnu-build-system))

       #:phases (modify-phases %standard-phases
                  ,@(if (system-hurd?)
                        `((add-after 'unpack
                                     'disable-multi-processing
                                     (lambda _
                                       (substitute* "Makefile.pre.in"
                                         (("-j0")
                                          "-j1")))))
                        '())
                  (add-before 'configure 'patch-lib-shells
                    (lambda _
                      ;; This variable is used in setup.py to enable cross compilation
                      ;; specific switches. As it is not set properly by configure
                      ;; script, set it manually.
                      ,@(if (%current-target-system)
                            '((setenv "_PYTHON_HOST_PLATFORM" ""))
                            '())
                      ;; Filter for existing files, since some may not exist in all
                      ;; versions of python that are built with this recipe.
                      (substitute* (filter file-exists?
                                           '("Lib/subprocess.py"
                                             "Lib/popen2.py"
                                             "Lib/distutils/tests/test_spawn.py"
                                             "Lib/test/support/__init__.py"
                                             "Lib/test/test_subprocess.py"))
                        (("/bin/sh")
                         (which "sh")))))
                  (add-before 'configure 'do-not-record-configure-flags
                    (lambda* (#:key configure-flags #:allow-other-keys)
                      ;; Remove configure flags from the installed '_sysconfigdata.py'
                      ;; and 'Makefile' so we don't end up keeping references to the
                      ;; build tools.
                      ;;
                      ;; Preserve at least '--with-system-ffi' since otherwise the
                      ;; thing tries to build libffi, fails, and we end up with a
                      ;; Python that lacks ctypes.
                      (substitute* "configure"
                        (("^CONFIG_ARGS=.*$")
                         (format #f "CONFIG_ARGS='~a'\n"
                                 (if (member "--with-system-ffi"
                                             configure-flags)
                                     "--with-system-ffi" ""))))))
                  (add-before 'check 'pre-check
                    (lambda _
                      ;; 'Lib/test/test_site.py' needs a valid $HOME
                      (setenv "HOME"
                              (getcwd))))
                  (add-after 'unpack 'set-source-file-times-to-1980
                    ;; XXX One of the tests uses a ZIP library to pack up some of the
                    ;; source tree, and fails with "ZIP does not support timestamps
                    ;; before 1980".  Work around this by setting the file times in the
                    ;; source tree to sometime in early 1980.
                    (lambda _
                      (let ((circa-1980 (* 10 366 24 60 60)))
                        (ftw "."
                             (lambda (file stat flag)
                               (utime file circa-1980 circa-1980) #t)))))
                  (add-after 'unpack 'remove-windows-binaries
                    (lambda _
                      ;; Delete .exe from embedded .whl (zip) files
                      (for-each (lambda (whl)
                                  (let ((dir "whl-content")
                                        (circa-1980 (* 10 366 24 60 60)))
                                    (mkdir-p dir)
                                    (with-directory-excursion dir
                                      (let ((whl (string-append "../" whl)))
                                        (invoke "unzip" whl)
                                        (for-each delete-file
                                                  (find-files "." "\\.exe$"))
                                        (delete-file whl)
                                        ;; Reset timestamps to prevent them from ending
                                        ;; up in the Zip archive.
                                        (ftw "."
                                             (lambda (file stat flag)
                                               (utime file circa-1980
                                                      circa-1980) #t))
                                        (apply invoke "zip" "-X" whl
                                               (find-files "."
                                                           #:directories? #t))))
                                    (delete-file-recursively dir)))
                                (find-files "Lib/ensurepip" "\\.whl$"))))
                  (add-after 'install 'remove-tests
                    ;; Remove 25 MiB of unneeded unit tests.  Keep test_support.*
                    ;; because these files are used by some libraries out there.
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
                        (match (scandir (string-append out "/lib")
                                        (lambda (name)
                                          (string-prefix? "python" name)))
                          ((pythonX.Y)
                           (let ((testdir (string-append out "/lib/" pythonX.Y
                                                         "/test")))
                             (with-directory-excursion testdir
                               (for-each delete-file-recursively
                                         (scandir testdir
                                                  (match-lambda
                                                    ((or "." "..")
                                                     #f)
                                                    ("support" #f)
                                                    (file (not (string-prefix?
                                                                "test_support."
                                                                file))))))
                               (call-with-output-file "__init__.py"
                                 (const #t))))
                           (let ((libdir (string-append out "/lib/" pythonX.Y)))
                             (for-each (lambda (directory)
                                         (let ((dir (string-append libdir "/"
                                                                   directory)))
                                           (when (file-exists? dir)
                                             (delete-file-recursively dir))))
                                       '("email/test" "ctypes/test"
                                         "unittest/test"
                                         "tkinter/test"
                                         "sqlite3/test"
                                         "bsddb/test"
                                         "lib-tk/test"
                                         "json/tests"
                                         "distutils/tests"))))))))
                  (add-after 'remove-tests 'move-tk-inter
                    (lambda* (#:key outputs inputs #:allow-other-keys)
                      ;; When Tkinter support is built move it to a separate output so
                      ;; that the main output doesn't contain a reference to Tcl/Tk.
                      (let ((out (assoc-ref outputs "out"))
                            (tk (assoc-ref outputs "tk")))
                        (when tk
                          (match (find-files out "tkinter.*\\.so")
                            ((tkinter.so)
                             ;; The .so is in OUT/lib/pythonX.Y/lib-dynload, but we
                             ;; want it under TK/lib/pythonX.Y/site-packages.
                             (let* ((len (string-length out))
                                    (target (string-append tk "/"
                                                           (string-drop (dirname
                                                                         (dirname
                                                                          tkinter.so))
                                                                        len)
                                                           "/site-packages")))
                               (install-file tkinter.so target)
                               (delete-file tkinter.so))))
                          ;; Remove explicit store path references.
                          (let ((tcl (assoc-ref inputs "tcl"))
                                (tk (assoc-ref inputs "tk")))
                            (substitute* (find-files (string-append out "/lib")
                                          "^(_sysconfigdata_.*\\.py|Makefile)$")
                              (((string-append "-L" tk "/lib"))
                               "")
                              (((string-append "-L" tcl "/lib"))
                               "")))))))
                  (add-after 'move-tk-inter 'move-idle
                    (lambda* (#:key outputs #:allow-other-keys)
                      ;; when idle is built, move it to a separate output to save some
                      ;; space (5MB)
                      (let ((out (assoc-ref outputs "out"))
                            (idle (assoc-ref outputs "idle")))
                        (when idle
                          (for-each (lambda (file)
                                      (let ((target (string-append idle
                                                                   "/bin/"
                                                                   (basename
                                                                    file))))
                                        (install-file file
                                                      (dirname target))
                                        (delete-file file)))
                                    (find-files (string-append out "/bin")
                                                "^idle"))
                          (match (find-files out "^idlelib$"
                                             #:directories? #t)
                            ((idlelib)
                             (let* ((len (string-length out))
                                    (target (string-append idle "/"
                                                           (string-drop
                                                            idlelib len)
                                                           "/site-packages")))
                               (mkdir-p (dirname target))
                               (rename-file idlelib target))))))))
                  (add-after 'move-idle 'rebuild-bytecode
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
                        ;; Disable hash randomization to ensure the generated .pycs
                        ;; are reproducible.
                        (setenv "PYTHONHASHSEED" "0")

                        (for-each (lambda (output)
                                    ;; XXX: Delete existing pycs generated by the build
                                    ;; system beforehand because the -f argument does
                                    ;; not necessarily overwrite all files, leading to
                                    ;; indeterministic results.
                                    (for-each (lambda (pyc)
                                                (delete-file pyc))
                                              (find-files output "\\.pyc$"))

                                    (apply invoke
                                           `(,,(if (%current-target-system)
                                                   "python3"
                                                   '(string-append out
                                                     "/bin/python3")) "-m"
                                             "compileall"
                                             "-o"
                                             "0"
                                             "-o"
                                             "1"
                                             "-o"
                                             "2"
                                             "-f" ;force rebuild
                                             "--invalidation-mode=unchecked-hash"
                                             ;; Don't build lib2to3, because it's
                                             ;; Python 2 code.
                                             "-x"
                                             "lib2to3/.*"
                                             ,output)))
                                  (map cdr outputs)))))
                  (add-before 'check 'set-TZDIR
                    (lambda* (#:key inputs native-inputs #:allow-other-keys)
                      ;; test_email requires the Olson time zone database.
                      (setenv "TZDIR"
                              (string-append (assoc-ref (or native-inputs
                                                            inputs) "tzdata")
                                             "/share/zoneinfo"))))
                  (add-after 'install 'install-sitecustomize.py
                    ,(customize-site version)))))
    (inputs (list bzip2
                  expat
                  gdbm
                  libffi ;for ctypes
                  sqlite ;for sqlite extension
                  openssl
                  readline
                  zlib
                  tcl
                  tk)) ;for tkinter
    (native-inputs `(("tzdata" ,tzdata-for-tests)
                     ("unzip" ,unzip)
                     ("zip" ,(@ (gnu packages compression) zip))
                     ("pkg-config" ,pkg-config)
                     ("sitecustomize.py" ,(local-file (search-auxiliary-file
                                                       "python/sitecustomize.py")))
                     ;; When cross-compiling, a native version of Python itself is needed.
                     ,@(if (%current-target-system)
                           `(("python" ,this-package)
                             ("which" ,which))
                           '())))
    (native-search-paths
     (list (guix-pythonpath-search-path version)
           ;; Used to locate tzdata by the zoneinfo module introduced in
           ;; Python 3.9.
           (search-path-specification
            (variable "PYTHONTZPATH")
            (files (list "share/zoneinfo")))))
    (home-page "https://www.python.org")
    (synopsis "High-level, dynamically-typed programming language")
    (description
     "Python is a remarkably powerful dynamic programming language that
is used in a wide variety of application domains.  Some of its key
distinguishing features include: clear, readable syntax; strong
introspection capabilities; intuitive object orientation; natural
expression of procedural code; full modularity, supporting hierarchical
packages; exception-based error handling; and very high level dynamic
data types.")
    (properties '((cpe-name . "python")))
    (license license:psfl)))
