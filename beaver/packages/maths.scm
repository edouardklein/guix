(define-module (beaver packages maths)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 match)
  #:use-module (gnu packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module ((guix build utils) #:select (alist-replace))
  #:use-module (guix build-system ant)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system dune)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system ocaml)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix build-system ruby)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages calendar)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages coq)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages cyrus-sasl)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages datamash)
  #:use-module (gnu packages dbm)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages file)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fltk)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gd)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages image-processing)
  #:use-module (gnu packages java)
  #:use-module (gnu packages less)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages netpbm)
  #:use-module (gnu packages ocaml)
  #:use-module (gnu packages onc-rpc)
  #:use-module (gnu packages parallel)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages prolog)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages tbb)
  #:use-module (gnu packages scheme)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages simulation)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages time)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages wxwidgets)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xml)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26))

(define-public minizinc-2.8
  (package
    (name "minizinc")
    (version "2.8.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/MiniZinc/libminizinc")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1h3pc92m7ypvmhrrhk2263ib5rqc4fv4n6lai0p8dwiidjpcqnpz"))
              (modules '((guix build utils)
                         (ice-9 ftw)
                         (srfi srfi-1)))
              (snippet
               '(begin
                  ;; Do not advertise proprietary solvers
                  (with-directory-excursion "cmake/targets"
                    (let ((targets '("libminizinc_fzn.cmake"
                                     "libminizinc_gecode.cmake"
                                     "libminizinc_mip.cmake"
                                     "libminizinc_nl.cmake"
                                     "libminizinc_osicbc.cmake"
                                     "libminizinc_parser.cmake"
                                     "libmzn.cmake"
                                     "minizinc.cmake"
                                     "mzn2doc.cmake")))
                     (for-each delete-file
                              (remove
                               (lambda (file)
                                 (member file (cons* "." ".." targets)))
                               (scandir ".")))
                    (substitute* "libmzn.cmake"
                      (("include\\(cmake/targets/(.*)\\)" all target)
                       (if (member target targets) all "")))))
                  (with-directory-excursion "include/minizinc/solvers/MIP"
                    (for-each delete-file
                              (remove
                               (lambda (file)
                                 (member file '("." ".."
                                                "MIP_osicbc_solverfactory.hh"
                                                "MIP_osicbc_wrap.hh"
                                                "MIP_solverinstance.hh"
                                                "MIP_solverinstance.hpp"
                                                "MIP_wrap.hh")))
                               (scandir "."))))
                  (with-directory-excursion "solvers/MIP"
                    (for-each delete-file
                              (remove
                               (lambda (file)
                                 (member file '("." ".."
                                                "MIP_osicbc_solverfactory.cpp"
                                                "MIP_osicbc_wrap.cpp"
                                                "MIP_solverinstance.cpp"
                                                "MIP_wrap.cpp")))
                               (scandir "."))))
                  (substitute* "CMakeLists.txt"
                    (("find_package\\(([^ ]*).*\\)" all pkg)
                     (if (member pkg '("Gecode" "OsiCBC" "Threads"))
                         all
                         "")))
                  ;; TODO: swap out miniz for zlib
                  #t))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f ; no ‘check’ target
       #:modules ((guix build cmake-build-system)
                  (guix build utils)
                  (srfi srfi-1))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'install-solver-configs
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((gecode (assoc-ref inputs "gecode"))
                   (pkgdatadir (string-append (assoc-ref outputs "out")
                                                  "/share/minizinc")))
               (call-with-output-file (string-append pkgdatadir
                                                     "/Preferences.json")
                 (lambda (port)
                   (display "\
{
  \"tagDefaults\": [
    [\"\", \"org.gecode.gecode\"],
    [\"gecode\", \"org.gecode.gecode\"]
  ],
  \"solverDefaults\": []
}"
                            port)
                   (newline port)))

               (mkdir-p (string-append pkgdatadir "/solvers"))
               (call-with-output-file (string-append pkgdatadir
                                                     "/solvers/gecode.msc")
                 (lambda (port)
                   (format port
                    "\
{
  \"id\": \"org.gecode.gecode\",
  \"name\": \"Gecode\",
  \"description\": \"Gecode FlatZinc executable\",
  \"version\": ~s,
  \"mznlib\": ~s,
  \"executable\": ~s,
  \"supportsMzn\": false,
  \"supportsFzn\": true,
  \"needsSolns2Out\": true,
  \"needsMznExecutable\": false,
  \"needsStdlibDir\": false,
  \"isGUIApplication\": false
}"
                    (last (string-split gecode #\-))
                    (string-append gecode "/share/gecode/mznlib")
                    (string-append gecode "/bin/fzn-gecode"))
                   (newline port)))))))))
    (native-inputs
     (list bison flex))
    (inputs
     (list cbc gecode zlib))
    (home-page "https://www.minizinc.org")
    (synopsis "High-level constraint modeling language")
    (description "MiniZinc is a high-level modeling language for constraint
satisfaction and optimization problems.  Models are compiled to FlatZinc, a
language understood by many solvers.")
    (license license:mpl2.0)))
