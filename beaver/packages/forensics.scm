(define-module (beaver packages forensics)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages base)
  #:use-module ((guix licenses) #:prefix license:)
  )

(define-public libewf
  (package
    (name "libewf")
    (version "20231119")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/libyal/libewf/releases/download/"
                                  version
                                  "/libewf-experimental-" version ".tar.gz"))
              (sha256
               (base32
                "1drakfa06gn3fz5b41awm1m16wz0kann8ayiaz4yrc6sll8x827c"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'remove-tests
           (lambda _
             (invoke "pwd")
             (invoke "ls" "-l")
             (invoke "rm" "-r" "tests")
             (invoke "mkdir" "tests")
             (invoke "sh" "-c" "echo all: > tests/Makefile")
             (invoke "sh" "-c" "echo >> tests/Makefile")
             (invoke "sh" "-c" "echo install: >> tests/Makefile"))))))
    (inputs (list fuse-2))
    (synopsis "Access the Expert Witness Compression Format (EWF).")
    (home-page "https://github.com/libyal/libewf")
    (description
     "The libewf package contains the following tools:

* ewfacquire; which writes storage media data from devices and files to EWF files.
* ewfacquirestream; which writes data from stdin to EWF files.
* ewfdebug; experimental tool does nothing at the moment.
* ewfexport; which exports storage media data in EWF files to (split) RAW format or a specific version of EWF files.
* ewfinfo; which shows the metadata in EWF files.
* ewfmount; which FUSE mounts EWF files.
* ewfrecover; special variant of ewfexport to create a new set of EWF files from a corrupt set.
* ewfverify; which verifies the storage media data in EWF files.")
    (license license:lgpl3+)))
