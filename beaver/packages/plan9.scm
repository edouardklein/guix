(define-module (beaver packages plan9)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (gnu system)
  #:use-module (gnu system setuid)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (beaver packages unix)
  #:use-module (gnu packages base)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-check)
  #:use-module (gnu packages golang-web)
  #:use-module (gnu packages golang-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages xorg))

(define-public suc
  (let ((revision "5")
        (commit "c14f3cae6c3032d363f0a5617ea49ec1d84b2812"))
    (package
      (name "suc")
      (version (git-version "20230719" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/edouardklein/suc")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1a4w39cs8caa83wzhn75lk6zmr3626axqhzc87q11sig44m0pbhg"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f                    ; no tests
         #:phases
         (modify-phases %standard-phases
           (add-after
               'configure
               'fix-path ; replace the hard coded path to suc.sh
               (lambda* (#:key inputs outputs #:allow-other-keys)
                 (let* ((out (assoc-ref outputs "out"))
                        (bin (string-append out "/bin")))
                   (substitute* "suc_wrapper.c"
                     (("/usr/bin/suc.sh")
                      (string-append bin "/suc.sh"))))))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin")))
                 ;; Those chmod won't be respected in the store anyway
                 ;; (the store is read-only, and you can't setuid a binary in it)
                 ;; but this is the spirit of upstream's makefile target "install"
                 (chmod "suc.sh" #o644)
                 (install-file "suc.sh" bin)
                 (chmod "usuc" #o754)
                 (install-file "usuc" bin)
                 (chmod "suc" #o4754)
                 (install-file "suc" bin)))))))
      (propagated-inputs
       (list rlwrap python-pygments))
      (native-inputs
       (list which))
      (inputs
       (list bash-minimal coreutils))
        ;which perl))
        ;libx11 libxt))
      (synopsis "The Simple Unix Chat")
      (home-page "https://gitlab.com/edouardklein/suc")
      (description
       "SUC is the simplest implementation of a chat system, relying on the kernel for authentication, access control, and synchronization.

The core is basically a few lines of bash.")
      (license license:expat))))

(define-public go-github-com-u-root-uio-ulog
  (package
    (name "go-github-com-u-root-uio-ulog")
    (version "0.0.0-20230305220412-3e8cd9d6bf63")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/u-root/uio")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1gvv66ixkgwikjx8sjdknvrmd08wv2ia02q8n8y3mnkhrgyyx1yf"))))
    (build-system go-build-system)
    (arguments
     (list
      #:import-path "github.com/u-root/uio/ulog"
      #:unpack-path "github.com/u-root/uio"
      #:install-source? #t
      ;; Source-only package
      #:tests? #f
      #:phases
      #~(modify-phases %standard-phases
          ;; Source-only package
          (delete 'build))))
    (home-page "https://github.com/u-root/uio")
    (synopsis "uio")
    (description #f)
    (license license:bsd-3)))

(define-public go-github-com-hugelgupf-p9-cmd-p9ufs
  (package
    (name "go-github-com-hugelgupf-p9-cmd-p9ufs")
    (version "0.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hugelgupf/p9")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ylbqh4in95xbj1pmdnxk3rhz0956gqg1m8sbivl30lyr6cmzphb"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/hugelgupf/p9/cmd/p9ufs"
       #:unpack-path "github.com/hugelgupf/p9"
       #:go ,go-1.20))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-u-root-uio-ulog" ,go-github-com-u-root-uio-ulog)))
    (home-page "https://github.com/hugelgupf/p9")
    (synopsis "p9")
    (description
     "p9 is a Golang 9P2000.L client and server originally written for @code{gVisor}.
p9 supports Windows, BSD, and Linux on most Go-available architectures.")
    (license license:asl2.0)))

(define-public 9mount
  (package
    (name "9mount")
    (version "1.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/sqweek/9mount/")
                    (commit "8009fdbb8022fb1320e297ee60024362e0df5618")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1kd93faadbqh667xs01nx3cm8a3j8qvgh2896s7nijqwim9n63zd"))))
    (build-system gnu-build-system)
    (arguments
     `(#:make-flags (list
                     "CC=gcc"
                     (string-append "prefix=" (assoc-ref %outputs "out")))
       #:phases
       (modify-phases %standard-phases
         ;; No configure is needed, no tests are provided
         ;; but the Makefile tries to chown the binaries
         ;; which can't be done at package definition,
         ;; as suid binaries can't exist in the store, but must
         ;; be done at os-definition.
         (replace 'configure
           (lambda _
             (substitute* "Makefile"
               (("chown root:users") "#chown root:users"))))
         (delete 'check))))
    (propagated-inputs `())
    (home-page "http://sqweek.net/code/9mount/")
    (synopsis "SUID mount tools to mount 9p filesystems.")
    (description
     "9mount is a set of SUID tools for mounting 9p filesystems via v9fs to help
cope with linux's poor mount support (I have to be root? Hostnames are only
resolved for nfs?). It also gives dial string support and defaults to 9p2000.

The tools offer a level of security - 9mount will only let you mount over
non-sticky directories you have write access to (no mounting over /tmp), and
9umount won't let you unmount partitions mounted by someone else, unless the
mount point happens to be in your home directory.")
    (license license:isc)))

(define-public mount.9p
  (let ((revision "1")
        (commit "2f7df72fb4b1a710673b2c9fc41122feb8db2e94"))
    (package
      (name "mount.9p")
      (version (git-version "20231002" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "git://the-dam.org/mount.9p")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "12659v27731zm32jldahqlrav1nyvvz0vdc4nzyxzpmz5bx0pgdd"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f                    ; no tests
         #:phases
         (modify-phases %standard-phases
           (add-after
               'configure
               'fix-path ; replace the hard coded path to suc.sh
               (lambda* (#:key inputs outputs #:allow-other-keys)
                 (let* ((out (assoc-ref outputs "out"))
                        (bin (string-append out "/bin")))
                   (substitute* "mount.9p_wrapper.c"
                     (("/usr/bin/mount.9p.sh")
                      (string-append bin "/mount.9p.sh"))))))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin")))
                 ;; Those chmod won't be respected in the store anyway
                 ;; (the store is read-only, and you can't setuid a binary in it)
                 ;; but this is the spirit of upstream's makefile target "install"
                 (chmod "mount.9p.sh" #o644)
                 (install-file "mount.9p.sh" bin)
                 (chmod "mount.9p" #o4754)
                 (install-file "mount.9p" bin)))))))
      (native-inputs
       (list which))
      (inputs
       (list bash-minimal coreutils))
      (synopsis "suid mount wrapper for 9p.")
      (home-page "https://the-dam.org/FIXME")
    (description "Mount requires privileges, but mounting 9P filesystems ought to be doable
by unprivileged users.

=mount.9p= is a setuid wrapper that allows unprivileged users to successfully
call mount -t 9p. It works the same way as mount.nfs.")
      (license license:agpl3+))))

(define-public go-github-com-julusian-godocdown
  (package
    (name "go-github-com-julusian-godocdown")
    (version "0.0.0-20170816220326-6d19f8ff2df8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Julusian/godocdown")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1qb8l77jlnrpx316s6chgygv62vgsalmf3rsrgxrissmngpg71an"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/robertkrimen/godocdown/godocdown"
       #:unpack-path "github.com/robertkrimen/godocdown"
       #:tests? #f))
    (home-page "https://github.com/Julusian/godocdown")
    (synopsis #f)
    (description #f)
    (license #f)))

(define-public go-github-com-robertkrimen-godocdown
  (package
    (name "go-github-com-robertkrimen-godocdown")
    (version "0.0.0-20130622164427-0bfa04905481")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/robertkrimen/godocdown")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0jgxv2y2anca4xp47lv4lv5n5dcfnfg78bn36vaqvg4ks2gsw0g6"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/robertkrimen/godocdown/godocdown"
       #:unpack-path "github.com/robertkrimen/godocdown"
       #:tests? #f))
    (home-page "https://github.com/robertkrimen/godocdown")
    (synopsis #f)
    (description #f)
    (license #f)))

(define-public go-github-com-stephens2424-writerset
  (package
    (name "go-github-com-stephens2424-writerset")
    (version "1.0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/stephens2424/writerset")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0609dxb6h9lncv09b7sr04kwwhgjv9kb476rzdww406mb7g0ffkh"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/stephens2424/writerset"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-robertkrimen-godocdown" ,go-github-com-robertkrimen-godocdown)
                         ("go-github-com-julusian-godocdown" ,go-github-com-julusian-godocdown)))
    (home-page "https://github.com/stephens2424/writerset")
    (synopsis "writerset")
    (description
     "Package writerset implements a mechanism to add and remove writers from a
construct similar to io.@code{MultiWriter}.")
    (license license:bsd-3)))

(define-public go-github-com-elazarl-go-bindata-assetfs
  (package
    (name "go-github-com-elazarl-go-bindata-assetfs")
    (version "1.0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/elazarl/go-bindata-assetfs")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "05j8gy417gcildmxa04m8ylriaakadr7zvwn2ggq56pdg7b63knc"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/elazarl/go-bindata-assetfs"))
    (home-page "https://github.com/elazarl/go-bindata-assetfs")
    (synopsis "go-bindata-assetfs")
    (description
     "assetfs allows packages to serve static content embedded with the go-bindata
tool with the standard net/http package.")
    (license license:bsd-2)))

(define-public go-github-com-dvyukov-go-fuzz
  (package
    (name "go-github-com-dvyukov-go-fuzz")
    (version "0.0.0-20231019021653-5581da83c52f")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dvyukov/go-fuzz")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "09a2g5v9i7jzcg55sxfs0jgdyz6ni3gmljp9q2k3hvxrb3fzp7ff"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/dvyukov/go-fuzz/go-fuzz-build"
       #:unpack-path "github.com/dvyukov/go-fuzz"))
    (home-page "https://github.com/dvyukov/go-fuzz")
    (synopsis "go-fuzz: randomized testing for Go")
    (description
     "Go-fuzz is a coverage-guided
@@url{http://en.wikipedia.org/wiki/Fuzz_testing,fuzzing solution} for testing of
Go packages.  Fuzzing is mainly applicable to packages that parse complex inputs
(both text and binary), and is especially useful for hardening of systems that
parse inputs from potentially malicious users (e.g. anything accepted over a
network).")
    (license license:asl2.0)))

(define-public go-bazil-org-fuse
  (package
    (name "go-bazil-org-fuse")
    (version "0.0.0-20230120002735-62a210ff1fd5")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/bazil/fuse")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "057l810cmbllmby6ds3x8pq4agpzm6dpg8hhh2ma3acnir3p84zx"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "bazil.org/fuse"
       #:tests? #f)) ;;Tests depend on fuse, too complex to make them work whithin the guix build container
    (propagated-inputs (list go-golang-org-x-xerrors go-golang-org-x-tools go-github-com-stephens2424-writerset
                             go-github-com-elazarl-go-bindata-assetfs go-github-com-tv42-httpunix go-golang-org-x-sys))
    (home-page "https://bazil.org/fuse")
    (synopsis "bazil.org/fuse -- Filesystems in Go")
    (description
     "Package fuse enables writing FUSE file systems on Linux and @code{FreeBSD}.")
    (license license:isc)))

(define-public go-github-com-hugelgupf-p9-linux
  (package
    (name "go-github-com-hugelgupf-p9-linux")
    (version "0.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hugelgupf/p9")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ylbqh4in95xbj1pmdnxk3rhz0956gqg1m8sbivl30lyr6cmzphb"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/hugelgupf/p9/linux"
       #:unpack-path "github.com/hugelgupf/p9"
       #:tests? #f
       #:go ,go-1.20))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-u-root-uio-ulog" ,go-github-com-u-root-uio-ulog)))
    (home-page "https://github.com/hugelgupf/p9")
    (synopsis "p9")
    (description
     "p9 is a Golang 9P2000.L client and server originally written for @code{gVisor}.
p9 supports Windows, BSD, and Linux on most Go-available architectures.")
    (license license:asl2.0)))

(define-public go-github-com-hugelgupf-p9-internal
  (package
    (name "go-github-com-hugelgupf-p9-internal")
    (version "0.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hugelgupf/p9")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ylbqh4in95xbj1pmdnxk3rhz0956gqg1m8sbivl30lyr6cmzphb"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/hugelgupf/p9/internal"
       #:unpack-path "github.com/hugelgupf/p9"
       #:tests? #f
       #:go ,go-1.20))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-u-root-uio-ulog" ,go-github-com-u-root-uio-ulog)))
    (home-page "https://github.com/hugelgupf/p9")
    (synopsis "p9")
    (description
     "p9 is a Golang 9P2000.L client and server originally written for @code{gVisor}.
p9 supports Windows, BSD, and Linux on most Go-available architectures.")
    (license license:asl2.0)))

(define-public go-github-com-hugelgupf-p9-vecnet
  (package
    (name "go-github-com-hugelgupf-p9-vecnet")
    (version "0.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hugelgupf/p9")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ylbqh4in95xbj1pmdnxk3rhz0956gqg1m8sbivl30lyr6cmzphb"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/hugelgupf/p9/vecnet"
       #:unpack-path "github.com/hugelgupf/p9"
       #:tests? #f
       #:go ,go-1.20))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-u-root-uio-ulog" ,go-github-com-u-root-uio-ulog)))
    (home-page "https://github.com/hugelgupf/p9")
    (synopsis "p9")
    (description
     "p9 is a Golang 9P2000.L client and server originally written for @code{gVisor}.
p9 supports Windows, BSD, and Linux on most Go-available architectures.")
    (license license:asl2.0)))

(define-public go-github-com-hugelgupf-p9-p9
  (package
    (name "go-github-com-hugelgupf-p9-p9")
    (version "0.3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hugelgupf/p9")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ylbqh4in95xbj1pmdnxk3rhz0956gqg1m8sbivl30lyr6cmzphb"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path "github.com/hugelgupf/p9/p9"
       #:unpack-path "github.com/hugelgupf/p9"
       #:tests? #f
       #:go ,go-1.20))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-u-root-uio-ulog" ,go-github-com-u-root-uio-ulog)))
    (home-page "https://github.com/hugelgupf/p9")
    (synopsis "p9")
    (description
     "p9 is a Golang 9P2000.L client and server originally written for @code{gVisor}.
p9 supports Windows, BSD, and Linux on most Go-available architectures.")
    (license license:asl2.0)))

(define-public the-dam-org-f29p
  (let ((revision "2")
        (commit "7672790b8e7cacd68e187acafb9110d3c2abbb18"))
    (package
      (name "the-dam-org-f29p")
      (version (git-version "20231130" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "git://the-dam.org/f29p")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "19q1y7yigaqlxw1aqqb395jn4036vf1l2zr60v930ziwfkkk4i56"))))
      (build-system go-build-system)
      (arguments
       `(#:import-path "the-dam.org/f29p"
         #:unpack-path "the-dam.org/f29p"
         #:go ,go-1.20))
    (propagated-inputs `(("go-bazil-org-fuse" ,go-bazil-org-fuse)
                         ("go-github-com-hugelgupf-p9-p9" ,go-github-com-hugelgupf-p9-p9)
                         ("go-github-com-hugelgupf-p9-internal" ,go-github-com-hugelgupf-p9-internal)
                         ("go-github-com-hugelgupf-p9-vecnet" ,go-github-com-hugelgupf-p9-vecnet)
                         ("go-github-com-hugelgupf-p9-p9" ,go-github-com-hugelgupf-p9-linux)))
    (home-page "https://the-dam.org")
    (synopsis "Fuse <-> 9P bridge")
    (description
     "In Linux' mount namespaces, one can only mount filesystems the devs have
deemed 'safe'. As of 2023 v9fs is not one of those whereas fuse is. This is a
fuse wrapper over 9P that will let you mount 9P filesystems in a mount
namespace.")
    (license license:agpl3+))))

(define-public listen
  (let ((revision "17")
        (commit "302bc25f73e5e72fc5d896843ee0ce8874af108b"))
    (package
      (name "listen")
      (version (git-version "20240912" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "git://the-dam.org/listen")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0f6fsglz275vrsdigmrqylb57frnpnshfm00vfak2p30fwaivpnd"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f ;no tests
         #:phases (modify-phases %standard-phases
                    (delete 'configure)
                    (replace 'install
                      (lambda* (#:key outputs #:allow-other-keys)
                        (let* ((out (assoc-ref outputs "out"))
                               (bin (string-append out "/bin")))
                          (chmod "listen" #o555)
                          (chmod "with-cap-bind" #o555)
                          (install-file "with-cap-bind" bin)
                          (install-file "listen.namespace" bin)
                          (install-file "listen" bin)))))))
      (propagated-inputs (list socat
                               coreutils
                               findutils
                               diffutils
                               bash
                               util-linux
                               inotify-tools
                               libcap-ng-static))
      (home-page "https://the-dam.org")
      (synopsis "Port of Plan 9's listen to Guix")
      (description
       "listen, works under the principle of redirecting network data to and
from the standard streams of a server script. The script is isolated in nested
guix containers. Server scripts allow for fine-grained (per port, per user)
access control to ports.")
      (license license:agpl3+))))

(define-public fingerd
  (let ((revision "4")
        (commit "892770e52be492b498d514ba3383c16d269857b8"))
    (package
      (name "fingerd")
      (version (git-version "20240311" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "git://the-dam.org/fingerd")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1aj2g8ja6f8kzj1k8m6jbmlia6n5h32125c6h3bbhdifw48xf1yh"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan '(("./" "bin/fingerd"))
         #:tests? #f)) ;no tests
      (home-page "https://the-dam.org")
      (synopsis "A finger server meant to work with listen")
      (description
       "This script parse requests coming on its standard input and calls the
appropriate script in /srv/finger.")
      (license license:agpl3+))))

(define-public u9fs
 (let ((revision "0")
       (commit "d65923fd17e8b158350d3ccd6a4e32b89b15014a"))
   (package
     (name "u9fs")
     (version (git-version "20240412" revision commit))
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/unofficial-mirror/u9fs.git")
                     (commit commit)))
               (file-name (git-file-name name version))
               (sha256
                (base32
                 "0h06l7ciikp3gzrr550z0fyrfp3y2067dfd3rxxw0q95z4l6vhy1"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f
       #:make-flags (list "CC=gcc" "LD=gcc")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
           ;; Easier to do it in scheme than to fix the original Makefile rule
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out  (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (man4 (string-append out "/share/man/man4")))
               (install-file "u9fs" bin)
               (mkdir-p man4)
               (copy-file "u9fs.man" (string-append man4 "/u9fs.4"))))))))
    (native-inputs (list))
    (synopsis "U9fs serves the Plan 9 protocol 9P from user-space on other operating systems.")
    (home-page "https://github.com/unofficial-mirror/u9fs")
    (description
     "The classic unix 9P server")
    (license license:bsd-4))))

(define-public diod
 (let ((revision "0")
       (commit "b4b5e8e00ed11b21d7fcf05a080dc054a8eac2d6"))
   (package
     (name "diod")
     (version (git-version "20240412" revision commit))
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/chaos/diod/")
                     (commit commit)))
               (file-name (git-file-name name version))
               (sha256
                (base32
                 "00bp3wi0dfyjckfs61nhfmfrncf8rbssa67qn1x7y7p1xkl7gz7n"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f
       #:make-flags (list "CC=gcc" "LD=gcc")
       #:phases
       (modify-phases %standard-phases
         (add-before 'bootstrap 'fake-git
           ;; Easier to do it in scheme than to fix the original Makefile rule
           (lambda* _
             (invoke "git" "init")
             (invoke "git" "add" ".")
             (invoke "git" "config" "user.email" "you@example.com")
             (invoke "git" "config" "user.name" "Your Name")
             (invoke "git" "commit" "-m" "osef"))))))
    (native-inputs
     (list autoconf automake libtool perl pkg-config git))
    (inputs (list ncurses lua))
    (synopsis "diod is a multi-threaded, user space file server that speaks 9P2000.L protocol.")
    (home-page "https://github.com/chaos/diod/")
    (description
     "diod")
    (license license:bsd-4))))

(define-public 9pfs
 (let ((revision "0")
       (commit "8b46ff1f802e44ef00c1dc0e498bf83f644e7c15"))
   (package
     (name "9pfs")
     (version (git-version "20240412" revision commit))
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/ftrvxmtrx/9pfs")
                     (commit commit)))
               (file-name (git-file-name name version))
               (sha256
                (base32
                 "0chy3yy4nfqw7l882i3qcq2fh1wxlwl5gpq97da1fl7ccrkgx1px"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f
       #:make-flags (list "CC=gcc" "LD=gcc")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-before 'build 'set-custom-flags
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((fuse-path (assoc-ref inputs "fuse")))
               (setenv "CFLAGS" (string-append "-I" fuse-path "/include/fuse3"))
               (setenv "LDFLAGS" (string-append "-L" fuse-path "/lib -lfuse")))))
         (replace 'install
           ;; Easier to do it in scheme than to fix the original Makefile rule
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out  (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (man1 (string-append out "/share/man/man1")))
               (install-file "9pfs" bin)
               (install-file "9pfs.1" man1)))))))
    (native-inputs (list fuse-2 pkg-config))
    (synopsis "9pfs mounts a 9P service using the FUSE file system driver. ")
    (home-page "https://github.com/unofficial-mirror/u9fs")
    (description
     "It is a replacement for 9pfuse from plan9port. Unlike 9pfuse, it works
equally well on Linux, OpenBSD, FreeBSD, and any other OS with a FUSE
implementation.")
    (license license:bsd-4))))


(define-public inferno
 (let ((revision "2")
       (commit "df575468ecf5f0cbbb154822f2fec4a6ad7eb2cb"))
   (package
     (name "inferno")
     (version (git-version "20240412" revision commit))
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://github.com/edouardklein/inferno-os")
                     (commit commit)))
               (file-name (git-file-name name version))
               (sha256
                (base32
                 "0jz13711f9z78zgl2fzyy1ckglbhzfjm50rpvm8xrxnz21xlf1fc"))))
    (build-system gnu-build-system)
    (arguments
      '(#:tests? #f
        #:system "i686-linux"
        #:phases
        (modify-phases %standard-phases
          (replace 'unpack
            (lambda* (#:key source outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (inferno (string-append out "/inferno")))
                (mkdir-p inferno)
                (copy-recursively source inferno)
                (chdir inferno)
                #t)))
          (replace 'patch-usr-bin-file
            (lambda* (#:key native-inputs inputs #:allow-other-keys)
              (let ((sh (string-append (assoc-ref inputs "bash") "/bin/sh")))
                (substitute* "makemk.sh"
                  (("#!/bin/sh") (string-append "#!" sh "\n")))
                (substitute* "mkfiles/mkhost-Linux"
                  (("/bin/sh") (string-append sh "\n")))
                (substitute* "utils/mk/Posix.c"
                  (("/bin/sh") sh)))))
          (delete 'patch-source-shebangs)
          (replace 'configure
            (lambda* (#:key outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (inferno (string-append out "/inferno")))
                (substitute* "mkconfig"
                  [("ROOT=.*")
                   (string-append "ROOT=" inferno "\n")]
                  [("SYSHOST=.*")
                   "SYSHOST=Linux\n"]
                  [("OBJTYPE=.*")
                   "OBJTYPE=386\n"]))))
          (delete 'patch-generated-file-shebangs)
          (replace 'build
            (lambda* (#:key outputs #:allow-other-keys)
              (invoke "./makemk.sh")
              (setenv "PATH" (string-append
                              (getcwd) "/Linux/386/bin:"
                              (getenv "PATH")))
              (invoke "mk" "nuke")
              (invoke "mk" "install")
              (mkdir-p "tmp")
              (mkdir-p "n")
              (mkdir-p "mnt")))
          (delete 'install))))
    (native-inputs (list libx11 libxext))
    (synopsis "Inferno represents services and resources in a file-like name hiearchy. ")
    (home-page "https://www.vitanuova.com/inferno/")
    (description
     "Inferno® is a distributed operating system, originally developed at Bell
Labs, but now developed and maintained by Vita Nuova® as Free
Software. Applications written in Inferno's concurrent programming language,
Limbo, are compiled to its portable virtual machine code (Dis), to run
anywhere on a network in the portable environment that Inferno
provides. Unusually, that environment looks and acts like a complete operating
system. Inferno represents services and resrouces in a file-like name
hiearchy. Programs access them using only the file operations open,
read/write, and close. `Files' are not just stored data, but represent
devices, network and protocol interfaces, dynamic data sources, and services.")
    (license license:bsd-4))))
