(define-module (beaver packages unix)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system ruby)
  #:use-module (guix git)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (gnu system)
  #:use-module (gnu system setuid)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages xorg))

(define-public finger
  (package
    (name "finger")
    (version "0.17")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://ftp.linux.org.uk/pub/linux/Networking/netkit/bsd-finger-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "1yhkiv0in588il7f84k2xiy78g9lv12ll0y7047gazhiimk5v244"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           ;; The ./configure doesn't ignore unknown
           ;; standard autotools options like CONFIG_SHELL.
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out")))
               (invoke "./configure"
                       (string-append "--prefix=" out)))))
         (add-after 'configure 'sub-old-imports
           (lambda _
             (substitute* '("finger/lprint.c" "finger/sprint.c")
               (("sys/time.h") "time.h"))))
         (replace 'install
           ;; Easier to do it in scheme than to fix the original Makefile rule
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out  (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (man1 (string-append out "/share/man/man1"))
                    (man8 (string-append out "/share/man/man8")))
               (install-file "finger/finger" bin)
               (install-file "finger/finger.1" man1)
               (install-file "fingerd/fingerd" bin)
               (install-file "fingerd/fingerd.8" man8)))))))
    (native-inputs (list))
    (synopsis "Find information about other users")
    (home-page "https://wiki.linuxfoundation.org/networking/netkit")
    (description
     "This is bsd-finger-0.17 for Linux.

This package updates bsd-finger-0.16.")
    (license license:bsd-4)))

(define-public permaudit
  (let ((revision "1")
        (commit "5bec3a0cb6de511f05e63d26a0b404db6f38dd1f"))
    (package
      (name "permaudit")
      (version (git-version "20230714" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "git://the-dam.org/permaudit")
            (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1b9s4zmch4vsx92birchwvzla9nws4fg4i9y017k7nda2kznvk6a"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f                    ; no tests
         #:phases
         (modify-phases %standard-phases
          (add-after 'configure 'fix-path ;; replace the hard coded path to
                                          ;; permaudit.sh
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let* ((out (assoc-ref outputs "out"))
                     (bin (string-append out "/bin")))
                (substitute* "permaudit_wrapper.c"
                  (("/usr/bin/permaudit.sh")
                   (string-append bin "/permaudit.sh"))))))
           (replace 'install            ; no install target
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin")))
                 ;; Those chmod won't be respected in the store anyway
                 ;; (the store is read-only, and you can't setuid a binary in it)
                 ;; but this is the spirit of upstream's makefile target "install"
                 (chmod "permaudit.sh" #o644)
                 (install-file "permaudit.sh" bin)
                 (chmod "permaudit" #o4754)
                 (install-file "permaudit" bin)
                 (mkdir-p (string-append out "/share/man/man1/"))
                 (copy-file "permaudit.1.man" (string-append out "/share/man/man1/permaudit.1"))))))))
      (inputs
       (list bash-minimal coreutils findutils sudo))
      (native-inputs
       (list emacs ;; org->man conversion
             which))
      (synopsis "Permission audit tool")
      (home-page "https://the-dam.org/docs/explanations/permaudit.html")
      (description
       "Permaudit lets you see who can read or write on the specified directory.")
      (license license:agpl3+))))

(define-public ruby-xdg-2.2
  (package
    (name "ruby-xdg")
    (version "2.2.5")
    (source
     (origin
       (method url-fetch)
       (uri (rubygems-uri "xdg" version))
       (sha256
        (base32 "04xr4cavnzxlk926pkji7b5yiqy4qsd3gdvv8mg6jliq6sczg9gk"))))
    (build-system ruby-build-system)
    (arguments
     `(#:tests? #f
       #:ruby ,ruby-3.2))
    (synopsis "A XDG Base Directory Specification implementation.")
    (description
     "This package provides a XDG Base Directory Specification implementation.")
    (home-page "https://alchemists.io/projects/xdg")
    (license #f)))

(define-public ruby-tmuxinator
  (package
    (name "ruby-tmuxinator")
    (version "3.0.5")
    (source
     (origin
       (method url-fetch)
       (uri (rubygems-uri "tmuxinator" version))
       (sha256
        (base32 "1ycsx9mvl0jsds4igi6avxclsyl5lndh1mpj2ykvzfz26wdddg6p"))))
    (build-system ruby-build-system)
    (arguments
     `(#:tests? #f
       #:ruby ,ruby-3.2))
    (propagated-inputs (list ruby-erubis ruby-thor ruby-xdg-2.2))
    (synopsis "Create and manage complex tmux sessions easily.")
    (description "Create and manage complex tmux sessions easily.")
    (home-page "https://github.com/tmuxinator/tmuxinator")
    (license license:expat)))

(define-public libcap-ng-static
  (package
    (inherit libcap-ng)
    (name "libcap-ng-static")
    (arguments
     `(#:configure-flags
       (list "--enable-static"
             "--without-python")))))

(define-public asp
  (package
    (name "asp")
    (version "3.5")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://alecmuffett.com/alecm/software/asp/asp%2C3.5.tar.gz"))
       (file-name "asp-3.5.tar.gz")
       (sha256
        (base32 "1xas5l737riba48afj586vga180dfl19gb4q4d2n20bkc5569k53"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f ;no tests
       #:phases (modify-phases %standard-phases
                  (replace 'configure
                    (lambda _
                      (substitute* "Makefile"
                        (("cc")
                         "gcc"))))
                  (replace 'build
                    (lambda _
                      (invoke "make" "asp" "CC=gcc")))
                  (replace 'install
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (bin (string-append out "/bin"))
                             (doc (string-append out "/share/doc/asp-readme.txt")))
                        (install-file "asp" bin)
                        (install-file "README" doc)))))))
    (home-page "https://alecmuffett.com/alecm/software/asp/")
    (synopsis "Asp is a simple, rule-based language for generating one-dimensional
animated .plan files.")
    (description
     "Asp is a simple, rule-based language for generating one-dimensional
animated '.plan' files that many people have written diddy programs to
generate.  Asp was desigend to do go 'one bit further' that those
programs usually do, and to be flexible with it.")
    (license #f)))
