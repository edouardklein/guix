(define-module (beaver functional-home-services)
   #:use-module (gnu home)
   #:use-module (gnu home services)
   #:use-module (gnu services)
   #:export (add-service extend-service modify-service remove-service
                         service-configuration))

(define syntax->string (compose symbol->string syntax->datum))

(define (service-configuration-stx stx service)
  "Return the syntax one can use to refer to home-xxx-configuration for the given
service"
  (datum->syntax stx (string->symbol
                      (string-append
                       "home-"
                       (syntax->string service)
                       "-configuration"))))

(define (service-type stx service)
  "Return the syntax one can use to refer to home-xxx-service-type for the given
service"
  (datum->syntax stx (string->symbol
                      (string-append
                       "home-"
                       (syntax->string service)
                       "-service-type"))))

(define-syntax add-service
  (lambda (stx)
    (syntax-case stx ()
      [(_ h service-name)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)  ;; It is wrapped in a lamba to make sure h is
               ;; evaluated once only. It it wasn't in a labmda, whatever
               ;; form h is in the calling code would be repeated
               ;; multiple times, and so if the form was e.g. (some-func
               ;; h), then some-func would be called multiple times,
               ;; which may not be desirable.
               (home-environment
                 (inherit x)
                 (services
                  (cons
                   (service service-type)
                   (home-environment-user-services x)))))
             h)))]
      [(_ h service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)]
         [service-configuration (service-configuration-stx stx #'service-name)])
        #'(begin
            ((lambda (x)  ;; Wrapping in a lambda for the same reasons as above
               (home-environment
                 (inherit x)
                 (services
                  (cons
                   (service service-type
                                 (service-configuration forms ...))
                   (home-environment-user-services x)))))
             h)))])))

(define-syntax extend-service
  (lambda (stx)
    (syntax-case stx ()
      [(_ h service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)
               (home-environment
                 (inherit x)
                 (services
                  (cons
                   (simple-service (format  #f "A ~a extension" (syntax->string #'service-name))
                                   service-type
                                   forms ...)
                   (home-environment-user-services x)))))
             h)))])))

(define-syntax service-configuration
  (lambda (stx)
    (syntax-case stx ()
      [(_ h service-name)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)
               (service-value
                (car
                 (filter
                  (lambda (service)
                    (eq? (service-kind service) service-type))
                  (home-environment-user-services x)))))
             h)))])))

(define-syntax modify-service
  (lambda (stx)
    (syntax-case stx (=>)
      [(_ h service-name conf-var => forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)]
         [service-configuration-stx (service-configuration-stx stx #'service-name)])
        #'(begin
            ((lambda (x)
               (let ([conf-var (service-configuration x service-name)])
                 (home-environment
                   (inherit x)
                   (services
                    (modify-services (home-environment-user-services x)
                      (service-type
                       config =>
                       (service-configuration-stx
                        (inherit config)
                        forms ...)))))))
             h)))]
      [(_ h service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)]
         [service-configuration (service-configuration-stx stx #'service-name)])
        #'(begin
            ((lambda (x)
               (home-environment
                 (inherit x)
                 (services
                  (modify-services (home-environment-user-services x)
                    (service-type
                     config =>
                     (service-configuration
                      (inherit config)
                      forms ...))))))
             h)))]
       )))

(define-syntax remove-service
  (lambda (stx)
    (syntax-case stx ()
      [(_ h service-name forms ...)
       (with-syntax
        ([service-type (service-type stx #'service-name)])
        #'(begin
            ((lambda (x)
               (home-environment
                 (inherit x)
                 (services
                  (modify-services (home-environment-user-services x)
                    (delete service-type)))))
             h)))])))
